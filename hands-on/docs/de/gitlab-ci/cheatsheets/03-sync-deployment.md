# Cheatsheet - Sync-Deployment

Alle Namen, die mit `my` anfangen können frei gewählt werden. Alle anderen Einträge sind definierte Keywords.

```yml
workflow: # Bedingung für die Ausführung der gesamten Datei
  rules:
    - ... # Liste von Regeln, wann die Pipeline ausgeführt werden soll

stages: # Reihenfolge der Stages
    - myFirstStage
    - mySecondStage

variables: # Variablem, die für alle Jobs gültig sind
    MyFirstVariable: ...
    MySecondVariable: ...

default:  # Defaults für alle Jobs in dieser Datei
    before_script: # Liste an Befehlen, die vor script ausgeführt werden
        - ...
    after_script:  # Liste an Befehlen, die nach script ausgeführt werden
        - ...

MyJob:
    image: ...   # Tag des Images, in dem alle Befehle des Jobs ausgeführt werden sollen
    stage: ...   # Referenz zu einer Stage aus "stages"
    needs:
        - ...    # Liste an Jobs, die vor diesem Jobs läufen müssen
    variables:   # Variable, die nur für diesen Job gültig ist
        MyJobVariable: ...
    rules:
        - if: ...            # Bedingung
          changes: ...       # Prüft, ob bestimmte Dateien verändert wurden
          exists: ...        # Prüft, ob bestimmte Dateien existieren
          allow_failure: ... # Job darf fehlschlagen
          variables: ...     # Variablen setzen
          when: ...          # "Wann" wird der Job ausgeführt
    before_script: # Liste an Befehlen, die vor script ausgeführt werden
        - ...
    script:
        - ...      # Liste an Befehlen
        - ...
    after_script:  # Liste an Befehlen, die nach script ausgeführt werden
        - ...
```


## Review Umgebung definieren

```yml
deploy_review:
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.example.com
    on_stop: stop_review_app
  script:
    - ...

stop_review_app:
  stage: stop_review
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  script:
    - ...
```

## SSH konfigurieren

```
myjob:
  script:
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y &&
        apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
```

Inhalt von `$SSH_KNOWN_HOSTS` generieren:

```
ssh-keyscan code-{ZAHL}.{{ domain }}
```