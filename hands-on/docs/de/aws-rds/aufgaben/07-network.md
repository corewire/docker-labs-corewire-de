# Netzwerk und Erreichbarkeit

!!! important "Ziele"
    - In diesem Projekt werden Sie das gesamte Netzwerk anlegen:
        - VPC, Subnet
        - Internet Gateway, Routing Table
        - Security Group
    - Sie werden eine EC2-Instanz und Datenbank starten und die Erreichbarkeit überprüfen

!!! help "Hilfsmittel"
    - Sollten Sie Probleme haben die Aufgaben zu lösen, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.
    - Versuchen Sie die Aufgaben erst einmal ohne die Lösungshilfen zu bearbeiten.

## Aufgabe 1 - VPC und Subnet anlegen

- Wir beginnen mit dem VPC und den Subnets
- Erstellen Sie ein VPC mit den folgenden Einstellungen
???+ summary "VPC Einstellungen"
    === "VPC settings"
        - Resources to create: **VPC only**
        - Name: **my-own-vpc**
        - IPv4 CIDR: **10.0.0.0/16**

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Gehen Sie in der Management Console auf den Service **VPC**
    - Im linken Menü finden Sie **Your VPCs**
    - Dort werden Ihnen die bestehenden VPCs angezeigt.
    - Erstellen Sie ein neues VPC über **Create VPC**
    - Erstellen Sie das VPC mit den Angaben von oben

- Erstellen Sie nun vier Subnets mit den folgenden Einstellungen
???+ summary "Subnet Einstellungen"
    === "Private subnet 1 settings"
        - VPC: **my-own-vpc**
        - Subnet Name: **my-own-private-subnet-1**
        - Availablitiy Zone: **eu-central-1a**
        - IPv4 CIDR block: **10.0.0.0/24**
    === "Public subnet 1 settings"
        - VPC: **my-own-vpc**
        - Subnet Name: **my-own-public-subnet-1**
        - Availablitiy Zone: **eu-central-1a**
        - IPv4 CIDR block: **10.0.1.0/24**
    === "Private subnet 2 settings"
        - VPC: **my-own-vpc**
        - Subnet Name: **my-own-private-subnet-2**
        - Availablitiy Zone: **eu-central-1b**
        - IPv4 CIDR block: **10.0.2.0/24**
    === "Public subnet 2 settings"
        - VPC: **my-own-vpc**
        - Subnet Name: **my-own-public-subnet-2**
        - Availablitiy Zone: **eu-central-1b**
        - IPv4 CIDR block: **10.0.3.0/24**

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Gehen Sie nun im linken Menü auf **Subnets** und dann auf **Create subnet**
    - Füllen Sie die Felder mit den Angaben von oben aus
    - Erstellen Sie die weiteren Subnets über **Add new subnet**
    - Erstellen Sie beide Subnets über **Create subnet**

- Sie haben nun ein VPC mit vier Subnets erstellt.

## Aufgabe 2 - Internet Gateway und Routing Table anlegen

- Erstellen Sie ein Internet Gateway mit dem Namen **my-own-igw**
- Fügen Sie das Internet Gateway Ihrem VPC hinzu

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Gehen Sie in der Management Console auf den Service **VPC**
    - Im linken Menü finden Sie **Internet Gateways**
    - Gehen Sie auf **Create internet gateway**
    - Tragen Sie den Namen ein und erstellen Sie das Internet Gateway
    - Sie landen auf der Detailseite zum Internet Gateway
    - Oben links finden Sie ein Dropdown **Actions**
    - Wählen Sie **Attach to VPC**
    - Wählen Sie Ihr VPC **my-own-vpc** aus und bestätigen Sie mit einem Klick auf **Attach internet gateway**

- Benennen Sie die bestehende Routing Table in ihrem VPC **my-own-private-rt**
- Fügen Sie eine explizite Subnet association zu **my-own-private-subnet-1** und **my-own-private-subnet-2** hinzu

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Im linken Menü finden Sie **Route Tables**
    - Wählen Sie die Route Table aus, die mit Ihrem VPC verbunden ist.
    - Wählen Sie rechts oben **Actions -> Manage Tags**
    - Fügen Sie das Tag **Name**=**my-own-private-rt** hinzu und speichern Sie
    - Wählen Sie nun wieder rechts oben **Actions -> Edit subnet association**
    - Wählen Sie **my-own-private-subnet-1** und **my-own-private-subnet-2** aus und klicken Sie **Save associations**

- Erstellen Sie eine zweite Route Table
???+ summary "Route Table Einstellungen"
    === "Route table settings"
        - Name: **my-own-public-rt**
        - VPC: **my-own-vpc**

- Fügen Sie eine Subnet Association zum Subnet **my-own-public-subnet-1** und **my-own-public-subnet-2** hinzu
- Fügen Sie eine Route von **0.0.0.0/0** zum Internet Gateway hinzu

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Im linken Menü finden Sie **Route Tables**
    - Klicken Sie auf **Create route table**
    - Erstellen sie die Route Table mit den Angaben von oben
    - Wählen Sie die Route Table in der Tabelle aus
    - Gehen Sie auf **Actions -> Edit subnet association**
    - Wählen Sie **my-own-public-subnet-1** und **my-own-public-subnet-2** aus und klicken Sie auf **Save associations**
    - Gehen Sie auf **Actions -> Edit routes**
    - Fügen Sie die oben genannte Route zum Internet Gateway hinzu
    - Mit dieser Route werden Instanzen in diesem Subnet vom Internet ereichbar

## Aufgabe 3 - Security Groups

- Erstellen Sie zwei Security Groups. Eine für die EC2 Instanz und eine für die Datenbank
???+ summary "Security Group Einstellungen"
    === "SG-1 Basic Details"
        - Name: **my-own-ec2-sg**
        - Description: **Allow SSH access to instance**
        - VPC: **my-own-vpc**
    === "SG-1 Inbound rules"
        - Type: **SSH**
        - Source: **Anywhere-IPv4**
    === "SG-2 Basic Details"
        - Name: **my-own-db-sg**
        - Description: **Allow access to database**
        - VPC: **my-own-vpc**
    === "SG-2 Inbound rules"
        - Type: **MYSQL/Aurora**
        - Source: **Custom**, **my-own-ec2-sg**

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Im linken Menü finden Sie weiter unten **SECURITY -> Security Groups**
    - Klicken Sie auf **Create security group**
    - Erstellen Sie die beiden Security Groups nacheinander mit den Angaben von oben

## Aufgabe 4 - Datenbank und EC2-Instanz erstellen

- Erstellen Sie eine RDS Subnet Group
???+ summary "Subnet Group Einstellungen"
    === "Subnet group details"
        - Name: **my-own-subnet-group**
        - Description: **My database subnet group**
        - VPC: **my-own-vpc**
    === "Add subnets"
        - Availablity Zones: **eu-central-1a** und **eu-central-1b**
        - Subnet: Private subnets (IP range **10.0.0.0/24** und **10.0.2.0/24**)

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Gehen Sie in der Management Console auf den Service **RDS**
    - Im linken Menü finden Sie **Subnet groups**
    - Klicken Sie auf **Create DB subnet group**
    - Erstellen Sie die Subnet Group mit den Angaben von oben

- Erstellen Sie eine MySQL-Aurora Datenbank
???+ summary "Datenbank Einstellungen"
    === "Engine options"
        - Engine type: **Amazon Aurora**
        - Edition: **Amazon Aurora MySQL-Compatible Edition**
    === "Templates"
        - Template: **Dev/Test**
    === "Settings"
        - DB cluster identifier: **my-own-db**
        - Master username: **admin**
        - Master password: Vergeben Sie ein Passwort und notieren Sie es sich.
    === "DB instance class"
        - DB instance class: **Burstable classes**
        - Type: **db.t3.small**
    === "Connectivity"
        - VPC: **my-own-vpc**
        - Subnet group: **my-own-subnet-group**
        - Security group: **my-own-db-sg**

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Gehen Sie in der Management Console auf den Service **RDS**
    - Im linken Menü finden Sie **Databases**
    - Klicken Sie auf **Create database**
    - Erstellen Sie die Datenbank mit den Angaben von oben

- Erstellen Sie eine EC2 Instanz
???+ summary "Ec2 Einstellungen"
    === "AMI"
        - Engine type: **Amazon Aurora**
        - AMI: **Amazon Linux 2 AMI - Kernel 5.10**
    === "Instance Type"
        - Type: **t2.micro**
    === "Instance Details"
        - Network: **my-own-vpc**
        - Subnet: **my-own-public-subnet-1**
        - Auto-assign Public IP: **Enable**
    === "Security Group"
        - **Select an existing security group**
        - Wählen Sie **my-own-ec2-sg**

- Erstellen Sie die EC2 Instanz ohne SSH Key

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Gehen Sie in der Management Console auf den Service **EC2**
    - Klicken Sie auf **Launch instance**
    - Erstellen Sie die EC2-Instanz mit den Angaben von oben


## Aufgabe 5 - Erreichbarkeit testen

- Warten Sie bis die EC2-Instanz verfügbar ist
- Verbinden Sie sich mit der EC2-Instanz über **EC2 Instance connect**

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Gehen Sie in der Management Console auf den Service **EC2**
    - Wählen Sie die Instanz im Dashboard aus
    - Klicken Sie oben rechts auf **Connect**
    - Klicken Sie im Tab **EC2 Instance connect** auf **Connect**

- Sie sind nun in einem Terminal auf der EC2 Instanz
- Installieren Sie den MySQL-Client mit folgendem Befehl:

```
sudo yum update && sudo yum install -y mysql
```

- Kopieren Sie sich die Datenbank URL
- Verbinden Sie sich auf die Datenbank mit

```
mysql -u {user} -p{password} -h {URL}
```

- Sie haben sich nun erfolgreich mit der Datenbank verbunden

## Ende

- Löschen Sie die Datenbank und die EC2 Instanz