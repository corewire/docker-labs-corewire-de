# Async-Deployment

!!! important "Ziel"
    In diesem Projekt geht es um:

    - Artefakte
    - Docker in der Pipeline
    - Gitlab Container Registry
    - Dependency-Management


!!! help "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der [Folien](https://slides.corewire.de/presentations/gitlab-ci/4-async-deployment/)
        und des [Cheatsheets](../cheatsheets/04-async-deployment.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Artefakte

### 1.1 Pipeline Editor öffnen

- Öffnen Sie den Pipeline-Editor für ihr `gitlab-ci-demoapp` Projekt.
- Löschen Sie den Inhalt der aktuelle `.gitlab-ci.yml`.
- Fügen Sie folgende Vorlage ein:

```yml
stages:
  - create
  - read

Create File:
  stage: create
  script:
    - echo "Hello World" >> ./result.txt

Read File:
  stage: read
  script:
    - ls
    - cat result.txt
```

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Den Pipeline-Editor finden Sie im Projekt-Menü auf der linken Seite unter:
        - **CI/CD ⇨ Editor**

### 1.2 Artefakt erstellen

- Erweitern Sie den Job "Create File" so, dass die Datei `result.txt` als Artefakt an die folgenden Jobs übergeben wird.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Der

    ```yml
    Create File:
        stage: create
        script:
            - echo "Hello World" >> ./result.txt
        artifacts:
            paths:
            - result.txt
    ```

### 1.3 Ergebnis prüfen

- Prüfen Sie, dass das Artefakt korrekt übergeben wurde inden Sie:
    - Die Logausgabe des Jobs "Read file" überprüfen
    - Die Datei über die Gitlab-Oberfläche herunterladen

## Aufgabe 2 - Container Registry

### 2.1 Vorbereitungen

- Löschen Sie den Inhalt der `gitlab-ci.yml` und beginnen Sie mit folgender Vorlage:

```yml
stages:
  - build
  - test
  - release

build:
  image: docker:20.10.16
  stage: build
  script:
    - echo building

test:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: test
  script:
    - echo testing

release:
  image: docker:20.10.16
  stage: release
  script:
    - echo releasing
```

### 2.2 Build-Stage

In dieser Teilaufgabe widmen wir uns der Build-Stage. Die Build-Stage soll folgende Schritte erledigen:

- Bei der Gitlab-Registry einloggen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    ```yml
            - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    ```

- Das Docker-Image bauen
- Das Docker-Image mit dem Tag `$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG` versehen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    ```yml
            - docker build --pull -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
    ```

- Das Docker-Image in die Registry pushen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Die Build-Stage sollte anschließend wie folgt aussehen:
    ```yml
    build:
        image: docker:20.10.16
        stage: build
        script:
            - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
            - docker build --pull -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
            - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    ```


### 2.3 Release-Stage

In dieser Teilaufgabe widmen wir uns der Release-Stage. Die Release-Stage soll folgende Schritte erledigen:

- Nur ausgeführt werden, wenn ein Git-Tag im Format `/^\d+\.\d+\.\d+$/` gesetzt ist

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    ```yml
        rules:
            - if: $CI_COMMIT_TAG =~ /^\d+\.\d+\.\d+$/
    ```

- Bei der Gitlab-Registry einloggen
- Das Image pullen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    ```yml
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    ```

- Das Image mit dem Tag `$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG` versehen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    ```yml
        - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    ```

- Das neue Tag in die Registry pushen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Die Release-Stage sollte anschließend wie folgt aussehen:

    ```yml
    release:
        image: docker:20.10.16
        stage: release
        script:
            - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
            - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
            - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
            - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
        rules:
            - if: $CI_COMMIT_TAG =~ /^\d+\.\d+\.\d+$/
    ```

### 2.4 Releases erstellen

Die Pipeline sollte bisher nur die Jobs "build" und "test" ausführen. Für die Release-Stage ist ein Git-Tag erforderlich.

Erstellen Sie die Git-Tags `2.0.0` und `2.0.1`. Für dieses Beispiel ist es nicht von Bedeutung, dass beide Tags auf den gleichen Commit und somit den gleichen Source Code zeigen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Git-Tags können Sie über das Menü links unter **Repository ⇨ Tags** erstellen.

### 2.5 Registry betrachten

- Jedes Tag sollte eine Pipeline mit 3 Jobs ausgelöst haben.
- Betrachten Sie das Ergebnis in der Container Registry
- Dort sollte das Image mit mehreren Docker-Tags enthalten sein

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Die Container Registry finden Sie über das Menü links unter **Packages and registries ⇨ Container Registry**.

!!! info "Hinweis"
    Es gibt Images mit den Tags `2.0.0` und `2-0-0`. Das liegt daran, dass für ein Git-Tag `2.0.0` die Variablen wie folgt gesetzt werden:

    - `$CI_COMMIT_REF_SLUG` = 2-0-0
    - `$CI_COMMIT_TAG` = 2.0.0

## (Optional) Aufgabe 3 - Dependency Management

In dieser Aufgabe wollen wir uns das Tool `Renovate`/`Renovatebot` für die Automatisierung des Dependency Management ansehen.

### 3.1 Renovate einrichten

- Betrachten Sie das Projekt `renovate-demo`
- Es enthält:
    - eine `config.json`, die Renovate Zugiff auf die private Gitlab Registry gibt
    - eine `.gitlab-ci.json`, die das Docker-Image `renovate/renovate` ausführt
- Damit der Renovate-Bot seine Arbeit verrichten kann, muss er noch konfiguriert werden. Dafür sind zwei Token notwendig:
    - `GITHUB_COM_TOKEN`: Zugriff auf Github für Release-Notes und Changelogs. Ist bereits instanzweit gesetzt.
    - `RENOVATE_TOKEN`: Muss noch in den CI-Variablen gesetzt werden

### 3.2 Personal Access Token

- Personal Access Token mit Scope "Api" erstellen
- Access Token als CI-Variable mit dem Namen `RENOVATE_TOKEN` anlegen
- Die Pipeline sollte nun erfolgreich durchlaufen

### 3.3 Projekt anlegen

- Um zu zeigen, wie Renovate funktioniert, benötigen wir ein Projekt, dass unser Demo-Image verwendet.
- Legen Sie ein neues Projekt an
- Öffnen Sie es in der Web IDE
- Erstellen Sie eine Datei `docker-compose.yml` mit folgendem Inhalt:

    ```
    version: '2'

    services:
    demo-project:
        image: 'registry.git.{{ domain }}/user-0/gitlab-ci-demoapp:2.0.0'
        ports:
        - "80:5000"
    ```
- Passen Sie den Image-Pfad an ihren User an
- Commiten Sie die Änderungen auf den Main-Branch

### 3.4 Renovate On-boarding

Neue Projekte müssen von Renovate durch das On-Boarding. Durch das Flag "--autodiscover" findet Renovate alle Projekte, die für ihn sichtbar sind.

- Führen Sie im Renovate-Projekt die Pipeline aus
- In ihrem erstellten Projekt wird dadurch ein Merge Request "Configure Renovate" erzeugt.
    - Dieser Merge Request legt eine Config-Datei an, in der man projektspezifische Einstellungen vornehmen kann.
- Mergen Sie den Merge Request
- Renovate ist nun für dieses Projekt aktiviert

### 3.5 Dependency Management

- Führen Sie erneut die Renovate-Pipeline aus.
- Betrachten Sie den Merge Request, den Renovate in ihrem Projekt erstellt hat.
- Für eine dauerhafte Nutzung eignet sich eine Scheduled Pipeline, die den Renovate Bot in einem regelmäßigen Intervall ausführt.