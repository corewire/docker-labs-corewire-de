# Cheatsheet 2 - Änderungen Rückgängig Machen

## Allgemein

| Befehl                    | Aktion                                                          |
|---------------------------|-----------------------------------------------------------------|
| ```git blame <datei>```   | Autor zu jeder Zeile in `<datei>` anzeigen                      |
| ```git commit --amend```  | Commit oder Commit-Nachricht korrigieren                        |
| ```git revert <commit>``` | Fügt einen Commit mit den inversen Änderungen von `<commit>` an |
| ```git reset <commit>```  | Setzt den Stand des Repositories auf `<commit>` zurück          |

## Git reset

| Befehl                             | Verändert Head | Verändert Index | Verändert Working Directory |
|------------------------------------|----------------|-----------------|-----------------------------|
| ```git reset --soft <commit>```    | ja             | nein            | nein                        |
| ```git reset [--mixed] <commit>``` | ja             | ja              | nein                        |
| ```git reset --hard <commit>```    | ja             | ja              | ja                          |

## Dateizustände

![Dateizustände](tikz/filestates.svg)
