# Umgang mit Images

!!! important "Ziel"
    In diesem Projekt geht es um den grundlegenden Umgang mit Docker Images.


!!! help "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der [Folien](https://slides.corewire.de/presentations/docker/03-images/)
    und des [Cheatsheets](../cheatsheets/02-images.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Images anzeigen lassen

- Lassen Sie sich die aktuellen Images auf dem System anzeigen.
- Sie sehen das Image für den VS Code Container, den Sie gerade nutzen und ein weiteres
für Traefik, einen Reverse-Proxy.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Die Images können mit
    ```shell
    docker image ls
    ```
    abgerufen werden.


## Aufgabe 2 - Verschiedene Images pullen

- Pullen Sie das Image `nginx` mit dem Tag `latest`.
- Pullen Sie das Image `nginx` mit dem Tag `mainline`.
    - Die Tags `latest` und `mainline` zeigen auf das gleiche Image. Docker merkt das und lädt nichts herunter.
- Pullen Sie das Image `nginx` mit dem Tag `stable`.
    - Hier wird ein Teil der Layer gecached.
- Lassen Sie sich die aktuellen Images auf dem System erneut anzeigen.
    - Die nginx Images sind jetzt mit den gepullten Tags verfügbar.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Die Images können mit
    ```shell
    docker pull <image>:<tag>
    ```
    gepullt werden.

## Aufgabe 3 - Demo-App in verschiedenen Versionen

- Starten Sie die Demo-Anwendung `corewire/docker-demoapp` mit dem Tag `1.1.1`.
- Rufen Sie die Demo-Anwendung im Browser in einem privaten Tab auf.
- Die Version wird entsprechend angezeigt.
- Starten Sie die Demo-Anwendung mit dem Tag `1.0`.
- Prüfen Sie im Browser, welche Version angezeigt wird.
- Starten Sie die Demo-Anwendung mit dem Tag `latest`.
- Prüfen Sie im Browser, welche Version angezeigt wird.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Um eine Demo-Anwendung zu starten, nutzen Sie folgenden Befehl:
    ```shell
    docker run -p 8080:5000 corewire/docker-demoapp:<tag>
    ```

## Aufgabe 4 - Images bauen

- Wechseln Sie in das Verzeichnis `docker-demoapp`.
- Bauen Sie das Docker Image mit:
```shell
docker build -t my-great-image .
```
- Lassen Sie sich die aktuellen Images auf dem System erneut anzeigen.
  - Das Image ist nun lokal verfügbar.

## Aufgabe 5 - Container Starten

- Starten Sie den Container mit:
```shell
docker run -p 8080:5000 my-great-image
```
- Rufen Sie die Demo-Applikation im Browser in einem privaten Tab auf. Sie ist
  über Ihre URL (`code-{ZAHL}.{{ domain }}`)
  auf dem Port 8080 zu erreichen. Verwenden Sie `http://` und nicht `https://`.