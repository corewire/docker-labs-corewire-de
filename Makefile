all:
	find ./hands-on -type f,l -name Makefile -execdir make all \;

clear:
	find ./hands-on -type f,l -name Makefile -execdir make clear \;

.PHONY: all clear
