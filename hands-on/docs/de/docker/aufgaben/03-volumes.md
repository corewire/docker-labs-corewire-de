# Umgang mit Volumes

!!! important "Ziel"
    In diesem Projekt geht es um Volumes. Sie sollen:

    - Die Demoanwendung mit unterschliedlichen Volume-Typen starten.
    - Die Unterschiede zwischen Managed Volumes und Bind Mounts kennen lernen.


!!! help "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der [Folien](https://slides.corewire.de/presentations/docker/04-volumes/)
    und des [Cheatsheets](../cheatsheets/03-volumes.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Vorbereitung

- Erstellen Sie einen Ordner `volumes`.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Falls die Explorer Leiste von VS Code links nicht bereits angezeigt wird, öffnen Sie  Explorer von VS Code mit `Strg + B` oder alternativ per Klick auf das Datei-Icon (über der Lupe) in der linken Leiste.
    - Vergewissern Sie sich, dass Sie sich im `workspace` Ordner befinden.
    - Erstellen Sie über den Explorer einen neuen Ordner mit dem Namen `volumes`.
    - Alternativ im Terminal: `mkdir volumes`. Achten Sie dabei auch wieder darauf, dass Sie sich im `workspace` Ordner befinden.

- Wechseln Sie im Terminal in den Ordner `volumes`.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - (Optional) Öffnen Sie ein Terminal (Menü > Terminal > New Terminal).
    - Wechseln Sie mit `cd volumes` in den erstellten Ordner. `cd` steht
      für `Change Directory`.
    - Führen Sie `pwd` aus. `pwd` liefert Ihnen das aktuelle Verzeichnis. Sie
      sollten die Ausgabe `/home/coder/workspace/volumes` erhalten.
    - Sollten Sie die Ausgabe nicht erhalten, wechseln Sie in das Verzeichnis mit
      `cd /home/coder/workspace/volumes`.


## Aufgabe 1 - Bind Mounts

### 1.1: Bind Mount erstellen

- Stellen Sie sicher, dass Sie sich im Verzeichnis `/home/coder/workspace/volumes` befinden.
- Starten Sie die Demo-Anwendung mit folgenden Eigenschaften:
    - im Hintergrund
    - mit Portforwarding (8080 -> 5000)
    - einem Bind Mount
        - Hostpfad: `${PWD}/data`
        - Containerpfad: `/app/data/notes`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Legen Sie den Ordner `data` mit `mkdir data` an und starten Sie das Image mit folgendem Befehl:
    ```
    docker run -d -p 8080:5000 --mount type=bind,source=${PWD}/data,target=/app/data/notes corewire/docker-demoapp
    ```
    - Alternativ mit `-v` und hierbei muss der Ordner vorher nicht erstellt werden:
    ```
    docker run -d -p 8080:5000 -v ${PWD}/data:/app/data/notes corewire/docker-demoapp
    ```

### 1.2: Demoanwendung überprüfen

- Besuchen Sie die Demo-Anwendung im Browser in einem privaten Tab. Sie ist über Ihre URL (`code-{ZAHL}.{{ domain }}`)
  auf dem Port 8080 zu erreichen. Verwenden Sie `http://` und nicht `https://`.
- Geben Sie 1-2 Notizen ein.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Versuchen Sie, die Webapp in einem neuen privaten Tab unter `http://code-0.{{ domain }}:8080/` zu öffnen.
    - Wichtig:
        - `http` **nicht** `https`
        - `code-0` durch Ihre Instanz ersetzen.

### 1.3: Bind Mount auf dem Host überprüfen

- Im Explorer in VS Code sehen Sie den Ordner `volumes/data`. Überprüfen Sie den Inhalt.
- Der Ordner enthält Ihre Notizen. Diese werden durch den Bind Mount auf dem Host-System persistiert.
- Bearbeiten Sie eine Notiz und speichern Sie die Datei.
- Laden Sie die Demoanwendung im anderen Tab mit F5 neu.
- Sie sollten Ihre geänderte Notiz sehen.


## Aufgabe 2 - Bind Mounts teilen

Sie haben in Aufgabe 1 erfolgreich einen Bind Mount angelegt. Die Notizen, sprich
der Zustand des Containers, werden nun unabhängig vom Container selber gespeichert.
Dadurch lässt sich der Container nun beliebig starten, stoppen und löschen, ohne dass
die Daten verloren gehen.

Im Folgenden wollen wir nun einen zweiten Container starten, der auf das gleiche
Verzeichnis zugreift.

### 2.1: Weitere Demoanwendung starten

- Stellen Sie sicher, dass Sie sich im Verzeichnis `/home/coder/workspace/volumes` befinden.
- Starten Sie die Demoanwendung mit folgenden Eigenschaften:
    - im Hintergrund
    - mit Portforwarding (**8081** -> 5000). **Wichtig:** Port 8080 ist bereits vergeben.
    - einem Bind Mount
        - Hostpfad: `${PWD}/data`
        - Containerpfad: `/app/data/notes`
        - Mode: **Readonly**

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Starten Sie das Image mit folgendem Befehl:
    ```
    docker run -d -p 8081:5000 --mount type=bind,source=${PWD}/data,target=/app/data/notes,readonly corewire/docker-demoapp
    ```
    Alternativ mit -v:
    ```
    docker run -d -p 8081:5000 -v ${PWD}/data:/app/data/notes:ro corewire/docker-demoapp
    ```

### 2.2: Demoanwendung überprüfen

- Besuchen Sie die Demo-Anwendung in einem neuen privaten Tab.
- Sie enthält dieselben Notizen wie die Demoanwendung auf Port 8080.


### 2.3: Notiz anlegen

- Versuchen Sie auf der Demoanwendung auf Port 8081 eine Notiz anzulegen.
- Sie sollten einen Fehler erhalten, da die Applikation keinen Schreibzugriff auf das Verzeichnis hat.
- Legen Sie eine Notiz in der Demoanwendung auf Port 8080 an.
- Laden Sie beide Demoanwendungen neu.

### 2.4: Demoanwendung stoppen

- Stoppen Sie die beiden Demoanwendungen.

!!! warning "VS Code Container"
    Die Container `code-server` und `vscode-traefik` stellen das VS Code Programm bereit.
    Diese Container **nicht** stoppen, da ansonsten die Instanz nicht mehr erreichbar ist.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie folgenden Befehl aus, um die laufenden Container angezeigt zu bekommen:
    ```
    docker ps
    ```
    - Suchen Sie nach den Einträgen mit `corewire/docker-demoapp` in der IMAGE-Spalte.
    - Kopieren Sie entweder die `CONTAINEER ID` am Anfang der Zeile oder den `NAME` am Ende der Zeile.
    - Stoppen Sie beide Container einzeln mit:
    ```
    docker stop <ID oder Name>
    ```


## Aufgabe 3 - Managed Volumes

Im Folgenden wollen wir das gleiche Szenario mit Managed Volumes umsetzen.

### 3.1: Demoanwendung mit Managed Volume starten

- Starten Sie die Demoanwendung mit folgenden Eigenschaften:
    - im Hintergrund
    - mit Portforwarding (8080 -> 5000)
    - einem Managed Volume
        - Volumename: `demoapp_notes`
        - Containerpfad: `/app/data/notes`


??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Starten Sie das Image mit folgendem Befehl:
    ```
    docker run -d -p 8080:5000 --mount source=demoapp_notes,target=/app/data/notes corewire/docker-demoapp
    ```
    Alternativ mit -v:
    ```
    docker run -d -p 8080:5000 -v demoapp_notes:/app/data/notes corewire/docker-demoapp
    ```

### 3.2: Demoanwendung überprüfen

- Besuchen Sie die Demo-Anwendung in einem neuen privaten Tab.
- Schreiben Sie 1-2 Notizen.

### 3.3: Volume anzeigen

Da das Volume nicht existiert hat, wurde es automatisch erzeugt. Lassen Sie sich
alle Volumes anzeigen. Sie sollten eine vergleichbare Ausgabe erhalten:

```
DRIVER    VOLUME NAME
local     demoapp_notes
```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Lassen Sie sich alle Volumes mit folgendem Befehl anzeigen:
    ```
    docker volume ls
    ```

Lassen Sie sich nun ebenfalls die low-level Details des Volumes anzeigen. Diese
enthalten zum Beispiel den Mountpoint unter `/var/lib/docker`.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Lassen Sie sich die Details mit folgendem Befehl anzeigen:
    ```
    docker volume inspect demoapp_notes
    ```

### 3.4: Weitere Demoanwendung starten

- Starten Sie die zweite Demoanwendung mit folgenden Eigenschaften:
    - im Hintergrund
    - mit Portforwarding (**8081** -> 5000). **Wichtig:** Port 8080 ist bereits vergeben.
    - einem Managed Volume
        - Volumename: `demoapp_notes`
        - Containerpfad: `/app/data/notes`
        - Mode: **Readonly**

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Starten Sie das Image mit folgendem Befehl:
    ```
    docker run -d -p 8081:5000 --mount source=demoapp_notes,target=/app/data/notes,readonly corewire/docker-demoapp
    ```
    Alternativ mit -v:
    ```
    docker run -d -p 8081:5000 -v demoapp_notes:/app/data/notes:ro corewire/docker-demoapp
    ```

### 3.5: Demoanwendung überprüfen

- Besuchen Sie beide Demoanwendungen.
- Versuchen Sie in beiden Demoanwendungen Notizen anzulegen.
- Das Verhalten sollte dem von vorher entsprechen:
    - Demoanwendung 1 kann neue Notizen anlegen.
    - Demoanwendung 2 kann die Noitzen nur anzeigen.

### 3.6: Demoanwendung stoppen

- Stoppen Sie, wie vorher auch, beide Demoanwendungen.

!!! warning "VS Code Container"
    Die Container `code-server` und `vscode-traefik` stellen das VS Code Programm bereit.
    Diese Container **nicht** stoppen, da ansonsten die Instanz nicht mehr erreichbar ist.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie folgenden Befehl aus, um die laufenden Container angezeigt zu bekommen:
    ```
    docker ps
    ```
    - Suchen Sie nach den Einträgen mit `corewire/docker-demoapp` in der IMAGE-Spalte.
    - Kopieren Sie entweder die `CONTAINEER ID` am Anfang der Zeile oder den `NAME` am Ende der Zeile.
    - Stoppen Sie beide Container einzeln mit:
    ```
    docker stop <ID oder Name>
    ```
