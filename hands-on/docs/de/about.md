# Impressum
Angaben gemäß § 5 TMG

Deurer, Durner, Löffler & Möhrle GbR

Rennweg 2

79106 Freiburg im Breisgau


## Vertreten durch:

Janosch Deurer

Jürgen Durner

Julian Löffler

Stefan Möhrle

# Kontakt

Telefon: +49 (0) 761 88 78 153 – 0

Telefax: +49 (0) 761 88 78 153 – 9

E-Mail: team@corewire.de

## Umsatzsteuer-ID
Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE325646446

## Verbraucherstreitbeilegung/Universalschlichtungsstelle
Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.
