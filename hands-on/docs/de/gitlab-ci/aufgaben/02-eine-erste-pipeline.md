# Eine erste Pipeline

!!! important "Ziel"
    In diesem Projekt geht es um erste Schritte mit der `.gitlab-ci.yml`. Sie sollen:

    - Jobs definieren
    - Abhängigkeiten zwischen Jobs abbilden
    - Sich mit (vordefinierten) Variablen vertraut machen


!!! help "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der [Folien](https://slides.corewire.de/presentations/gitlab-ci/2-erste-schritte/)
        und des [Cheatsheets](../cheatsheets/02-eine-erste-pipeline.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Jobs

### 1.1 Pipeline Editor öffnen

- Öffnen Sie den Pipeline-Editor für ihr `gitlab-ci-demoapp` Projekt.
- Die aktuelle `.gitlab-ci.yml` enthält nur ein einfaches Hello-World-Beispiel

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Den Pipeline-Editor finden Sie im Projekt-Menü auf der linken Seite unter:
        - **CI/CD ⇨ Editor**

### 1.2 Jobs anlegen

- Legen Sie folgende 5 weitere Jobs an:
    - Compile
    - Lint
    - Unittest
    - Integrationtest
    - Release
- Als Script genügt:
    - eine Ausgabe via `echo "Compiling..."`/`echo "Linting..."` etc.
    - ein `sleep 5`
- Löschen Sie das Hello-World-Beispiel

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Ihre `.gitlab-ci.yml` sollte anschließend wie folgt aussehen:
    ```yml
    Compile:
        script:
            - sleep 5
            - echo "Compiling..."

    Lint:
        script:
            - sleep 5
            - echo "Linting..."

    Unittest:
        script:
            - sleep 5
            - echo "Unittesting..."

    Integrationtest:
        script:
            - sleep 5
            - echo "Integrationtesting..."

    Release:
        script:
            - sleep 5
            - echo "Releasing..."
    ```


### 1.3 Pipeline ausführen

- Commiten Sie ihre Änderungen
- Betrachten Sie die Ausführung der Pipeline und die Logs der einzelnen Jobs

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Im Pipeline-Editor ganz unten können Sie eine Commit-Nachricht angeben
      und einen Commit mit ihren Änderungen über den Button "Commit changes" erstellen.
    - Über den Commit wird automatisch eine Pipeline getriggert
    - Schauen Sie sich die Ausführung der Pipeline/der einzelnen Jobs an. Informationen zur aktuellen Pipeline-Ausführung finden Sie an folgenden Stellen:
        - Im Pipeline-Editor ganz oben
        - Links in der Navigation **CI/CD ⇨ Pipelines**


## Aufgabe 2 - Stages

Die Jobs aus Aufgabe 1 laufen derzeit alle parallel. In dieser Aufgabe sollen sie
nun durch Stages gruppiert werden.

### 2.1 Stages anlegen

- Legen Sie folgende drei Stages an:
  - Build
  - Test
  - Release

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Fügen Sie folgende Zeilen oben in der `.gitlab-ci.yml` ein:
    ```yml
    stages:
        - Build
        - Test
        - Release
    ```


### 2.2 Jobs in Stages gruppieren

- Gruppieren Sie die Jobs so, dass sie den Stages wie folgt zugeordnet sind:
    - Build
        - Compile
        - Lint
    - Test
        - Unittest
        - Integrationtest
    - Release
        - Release

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Erweitern Sie die einzelnen Jobs mit dem `stage` Keyword nach folgendem Schema:
    ```yml
    Compile:
        stage: Build
        script:
            - sleep 5
            - echo "Compiling..."
    ```

### 2.3 Pipeline ausführen

- Commiten Sie ihre Änderungen
- Betrachten Sie die Ausführung der Pipeline
- Die Jobs sollten nun in Stages gruppiert sein und entsprechend nacheinander ausgeführt werden

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Im Pipeline-Editor ganz unten können Sie eine Commit-Nachricht angeben
      und einen Commit mit ihren Änderungen über den Button "Commit changes" erstellen.
    - Über den Commit wird automatisch eine Pipeline getriggert
    - Schauen Sie sich die Ausführung der Pipeline/der einzelnen Jobs an. Informationen zur aktuellen Pipeline-Ausführung finden Sie an folgenden Stellen:
        - Im Pipeline-Editor ganz oben
        - Links in der Navigation **CI/CD ⇨ Pipelines**

## Aufgabe 3 - Abhängigkeiten / Needs

### 3.1 Abhänigkeiten definieren

- Entfernen Sie die Definition und Zuweisung der Stages
- Definieren Sie folgende Abhängigkeiten
    - Integrationtest und Unittest benötigen Compile
    - Release benötigt Integrationtest und Unittest
- Erhöhen Sie den Sleep-Timer von Lint auf 60 Sekunden

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Ihre `.gitlab-ci.yml` sollte anschließend wie folgt aussehen:
    ```yml
    Compile:
        script:
            - sleep 5
            - echo "Compiling..."

    Lint:
        script:
            - sleep 60
            - echo "Linting..."

    Unittest:
        needs:
            - Compile
        script:
            - sleep 5
            - echo "Unittesting..."

    Integrationtest:
        needs:
            - Compile
        script:
            - sleep 5
            - echo "Integrationtesting..."

    Release:
        needs:
            - Unittest
            - Integrationtest
        script:
            - sleep 5
            - echo "Releasing..."
    ```


### 3.2 Pipeline ausführen

- Commiten Sie ihre Änderungen
- Betrachten Sie die Ausführung der Pipeline

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Im Pipeline-Editor ganz unten können Sie eine Commit-Nachricht angeben
      und einen Commit mit ihren Änderungen über den Button "Commit changes" erstellen.
    - Über den Commit wird automatisch eine Pipeline getriggert
    - Schauen Sie sich die Ausführung der Pipeline/der einzelnen Jobs an. Informationen zur aktuellen Pipeline-Ausführung finden Sie an folgenden Stellen:
        - Im Pipeline-Editor ganz oben
        - Links in der Navigation **CI/CD ⇨ Pipelines**

## Aufgabe 4 - Vordefinierte Variablen

### 4.1 Job anpassen

- Löschen Sie alle Jobs
- Erstellen Sie einen Job, der folgende vordefinierte Variablen ausgibt
    - `CI_PIPELINE_SOURCE`
    - `CI_PIPELINE_ID`
    - `CI_PIPELINE_IID`
- Suchen Sie sich in der Dokumentation 1-2 weitere Variablen heraus, die für Sie interessant sein könnten.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Ihre `.gitlab-ci.yml` sollte anschließend wie folgt aussehen:
    ```yml
    Demo:
        script:
            - echo $CI_PIPELINE_SOURCE
            - echo $CI_PIPELINE_ID
            - echo $CI_PIPELINE_IID
    ```

### 4.2 Pipeline ausführen

- Commiten Sie ihre Änderungen
- Betrachten Sie das Log ihres Jobs

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Im Pipeline-Editor ganz unten können Sie eine Commit-Nachricht angeben
      und einen Commit mit ihren Änderungen über den Button "Commit changes" erstellen.
    - Über den Commit wird automatisch eine Pipeline getriggert
    - Schauen Sie sich die Ausführung der Pipeline/der einzelnen Jobs an. Informationen zur aktuellen Pipeline-Ausführung finden Sie an folgenden Stellen:
        - Im Pipeline-Editor ganz oben
        - Links in der Navigation **CI/CD ⇨ Pipelines**

### 4.3 Pipeline manuell ausführen

- Führen Sie die Pipeline ein weiteres mal manuell aus.
- Betrachten Sie das Log ihres Jobs

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Links in der Navigation **CI/CD ⇨ Pipelines**
    - Oben Rechts über den Button "Run pipeline"

### 4.4 Pipeline per Schedule ausführen

- Erstellen Sie einen Schedule, der die Pipeline einmal am Tag ausführt.
- Führen Sie den Schedule manuell aus.
- Betrachten Sie das Log ihres Jobs

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Links in der Navigation **CI/CD ⇨ Schedules**
    - Oben Rechts über den Button "New schedule"
    - Wenn der Schedule erstellt ist, können Sie ihn in der Schedule-Übersicht manuell triggern

### 4.5 Schedule löschen

- Löschen Sie den Schedule wieder


## (Optional) Aufgabe 5 - Stages und Abhängigkeiten kombinieren

- Kombinieren Sie Stages und Abhängigkeiten wie folgt:
    - Stages und Jobzugehörigkeit wie in Aufgabe 2.2
    - Die Jobs Unittest und Integrationstest sollen von Compile abhängig sein
- Fügen Sie für einen bessere Sichtbarkeit ein `sleep 60` in das Script vom Lint-Job ein
- Erstellen Sie einen Commit und beobachten Sie den Effekt

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Ihre `.gitlab-ci.yml` sollte anschließend wie folgt aussehen:
    ```yml
    stages:
        - build
        - test
        - release

    Compile:
        stage: build
        script:
            - echo "Compiling..."

    Lint:
        stage: build
        script:
            - sleep 60
            - echo "Linting..."

    Unittest:
        stage: test
        needs:
            - Compile
        script:
            - echo "Unittesting..."

    Integrationtest:
        stage: test
        needs:
            - Compile
        script:
            - echo "Integrationtesting..."

    Release:
        stage: release
        script:
            - echo "Releasing..."
    ```
