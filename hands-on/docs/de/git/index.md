# Git - Dezentrale Versionierung

- [Zu den Aufgaben](./aufgaben/01-mein-erstes-projekt.md)
- [Zu den Cheatsheets](./cheatsheets/1-einfuehrung.md)
- [Zu den Folien](https://slides.corewire.de/presentations/git/1-introduction/#/)

## Dauer
2 Tage

## Zielgruppe
- Softwareentwickler:innen
- Systemadministrator:innen
- DevOps-Engineers

## Voraussetzungen
- (optional) Erste Erfahrungen mit dem Terminal/Bash
- (optional) Erste Erfahrungen mit Git

## Kursziel
Der Kurs vermittelt die Bedienung von Git in der Praxis. Am ersten Tag werden Grundlagen und Zusammenhänge für den täglichen Umgang mit Git behandelt. Der zweite Tag beinhaltet die Zusammenarbeit im Team und fortgeschrittene Funktionen, mit denen sie ein Verständnis für die technischen Hintergründe von Git entwickeln, um auch auf nicht behandelte Problemstellungen reagieren zu könnnen. Der Kurs richtet sich somit auch explizit an Teams mit unterschiedlichen Erfahrungsleveln zu Git.

## Schulungsform
Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **60% theoretischen Inhalten** und **40% praktischen Übungen**.

## Kursinhalt

- Grundkonzepte der dezentralen Versionierung
- Git commit/push/pull und weitere Grundlagen zu Git
- Branching, sowie Einordnung in Projektorganisation und Mergen von komplexeren Konflikten
- Rebase, Vergleich zu Merging und Best Practices inklusive Sonderfällen und Stolperfallen
- History Rewriting und Force Push, Gefahren und Potenziale, sowie Verwendung in Teamarbeit
- Nutzung von GitLab und vergleichbaren Oberflächen (GitHub) zur Teamorganisation und Projektverwaltung
- Branching Workflows wie Gitflow und Pull Requests, mit Praxisteilen in GitLab
- Fortgeschrittene Git-Features wie Cherry-picking, interaktiver Rebase, Bisect, Stash und mehr
