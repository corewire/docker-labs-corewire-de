# Erste Schritte

- Fokus: Erster Kontakt mit der `gitlab-ci.yml`. Fragen beantworten wie
    - "Was läuft da?"
    - "Wo läuft das?
- Pipeline-Architektur und Möglichkeiten sind noch nicht relevant.

## 2.1 Erster sinnvoller Job

- Issue und MR erstellen
- Web IDE verwenden um `gitlab-ci.yml` anzupassen

```yml
test:
  image: python
  script:
    - pytest demoapp
```
-> Job schlägt fehl

```yml
test:
  image: python
  script:
    - pip install pytest
    - pytest demoapp
```

- Daraus folgt:
    - Wo laufen Jobs?
    - Um was muss ich mich alles kümmern?

**Demo Ende**

## 2.2 Pwd, ls und Stages

```yml
test:
  image: python
  script:
    - pip install pytest
    - pytest demoapp

demo:
  image: python
  script:
    - pwd
    - ls -la
    - python --version

demo:
  image: python:3.9
  script:
    - pwd
    - ls -la
    - python --version
```

Namenskollision beabsichtigt. Bei gleichem Namen wird nur der letzte erzeugt.

-> `demo` -> `demo_1`,`demo_2`

## 2.3 Pipeline Editor

- Auf richtigen Branch wechseln
- Visualize und Validate zeigen mit `demo` und `demo_1`/`demo_2`

```
stages:
  - build
  - test
  - deploy

jobs...

deploy:
  stage: deploy
  script:
    - echo deploying...
```

- Stages zeigen
- Needs zeigen

```
run_build:
  script: echo building...

run_test:
  image: python
  needs:
    - run_build
  script:
    - pip install pytest
    - pytest demoapp

run_lint:
  needs:
    - run_build
  script:
    - 'echo TODO: run linter'

deploy:
  needs:
    - run_lint
    - run_test
  script:
    - echo deploying..
```

- Test anpassen, sodass Test fehlschlägt
- Jobs laufen nur, wenn vorher alles erfolgreich war

**Demo Ende**

## 2.4 Variables

```yml
variables:
  var_1: Hello

run_1:
  variables:
    var_2: World
  script:
    - echo $var_1
    - echo $var_2

run_2:
  variables:
    var_1: Hey
  script:
    - echo $var_1
    - echo $var_2
```

- Output der Jobs zeigen
- Pipeline manuell laufen lassen mit `var_2=Name`
- Predefined Variables zeigen

```yml
run:
  script:
    - env
```