# Cheatsheet 1 - Intro

| Command                                        | Action                                                                         |
|------------------------------------------------|--------------------------------------------------------------------------------|
| ```apt update && apt install git```            | Install git on a Linux based OS (Debian-derivates)                             |
| ```git clone <adress>```                       | Get a repository from a remote adress to the local disk                        |
| ```git status```                               | Shows wich Files are modified/staged/untracked                                 |
| ```git diff```                                 | Shows differences in files                                                     |
| ```git diff <file>```                          | Shows differences in a specific \<file\>                                       |
| ```git diff --staged```                        | Shows differences from files in the Stage-area                                 |
| ```git log```                                  | Shows the history                                                              |
| ```git log --graph```                          | Shows the history with a graph                                                 |
| ```git config <setting> <value>```             | Set a config value for the active repository                                   |
| ```git config --global <setting> <value>```    | Set a config value systemwide                                                  |
| ```git config --list```                        | Show all current settings and their values                                     |
| ```git add <path>```                           | Stage a file                                                                   |
| ```git restore --staged <path>```              | Unstage a file                                                                 |
| ```git restore <path>```                       | Restore a modified file to original state                                      |
| ```git commit```                               | Create a commit with all current staged files                                  |
| ```git mv <file1> <file2>```                   | Stage a file renaming                                                          |
| ```git rm <file>```                            | Stage a file deletion                                                          |
| ```git push```                                 | Sync the local commits to the remote server                                    |
| ```git pull```                                 | Get new commits from the server                                                |
| ```git help <command>```                       | Get help for the git commands                                                  |
