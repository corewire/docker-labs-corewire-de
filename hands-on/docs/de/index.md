---
title: Corewire Hands-On Collection
---

# Willkommen

Hier finden Sie sämtliche Unterlagen, die Sie für unsere Schulungen benötigen.


## Direkt zu den Schulungsunterlagen:
- [AWS RDS](./aws-rds/)

- [Docker & Container](./docker/)

- [Git - Dezentrale Versionierung](./git/)

- [Projektmanagement mit Gitlab](./gitlab/)

- [CI/CD-Pipelines mit Gitlab](./gitlab-ci/)
