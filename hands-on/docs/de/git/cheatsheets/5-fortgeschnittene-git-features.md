# Cheatsheet 5 - Fortgeschrittene Git Features

| Command                               | Action                                                                                 |
|---------------------------------------|----------------------------------------------------------------------------------------|
| ```git checkout HEAD~```              |                                                                                        |
| ```git checkout HEAD^```              | Wechselt zum Vorgänger von HEAD                                                        |
| ```git checkout main~2```           |                                                                                        |
| ```git checkout main^^```           | Wechselt zum Vor-Vorgänger von main                                                  |
|                                       |                                                                                        |
| ```git add -p```                      | Dateien interaktiv und nur teilweise stagen                                            |
|                                       |                                                                                        |
| ```git submodule add <url>```         | Submodul zu Repository hinzufügen                                                      |
| ```git submodule init```              | Submodule initialisieren (aktualisiert .gitmodules)                                    |
| ```git submodule update```            | Submodule aktualisieren, sodass sie auf dem erwarteten Stand des Parent-Projektes sind |
|                                       |                                                                                        |
| ```git bisect start```                | Bisect beginnen                                                                        |
| ```git bisect good <commit>```        | Markiert den initialen guten Commit                                                    |
| ```git bisect bad <commit>```         | Markiert den initialen schlechten Commit                                               |
| ```git bisect good```                 | Markiert den aktuellen Commit als gut                                                  |
| ```git bisect bad```                  | Markiert den aktuellen Commit als schlecht                                             |
| ```git bisect run <command>```        | Automatisierter lauf von bisect (Return code: good (== 0) or bad (!= 0)                |
|                                       |                                                                                        |
| ```git lfs install .```               | Installiert lfs für das aktuelle Repository                                            |
| ```git lfs track '*.png'```           | Trackt alle png-Dateien mit lfs                                                        |
|                                       |                                                                                        |
| ```git log -3```                      | Zeigt die letzten 3 Commits                                                            |
| ```git log --after="2019-7-1"```      | Zeigt die Commits nach "2019-7-1"                                                      |
| ```git log --author="Alice"```        | Zeigt Commits von der Autorin 'Alice' (Regex möglich)                                  |
| ```git log --grep="Closes #1337"```   | Zeigt Commits die die Commit-Nachricht "Closes #1337" enthalten                        |
| ```git log -- document.txt code.py``` | Zeigt Commits für die angegebene(n) Datei(en)                                          |
| ```git log main..feature```         | Zeigt den Unterschied zwischen Branch "main" und "feature" an                        |
| ```git log --no-merges```             | Zeigt Nicht-Merge Commits                                                              |
| ```git log --merges```                | Zeigt Merge Commits                                                                    |
