# Debugging

Das hier auschecken:
<https://gitlab.com/corewire/hands-on/docker-demoapp/-/merge_requests/4>

Dann folgende Probleme beheben:

## docker-compose
- Volume für den `database` service
  - db-volume -> database-volume

## Dockerfile

- apt install -y Flag fehlt
- fehlendes gcc in apt
- Tippfehler in ENV FLASK_APP=docker/app
  - ENV FLASK_APP=docker/app -> ENV FLASK_APP=dockerdemo/app
  
## app.py

- Syntaxfehler in Zeile 13

## Dockerfile

- Container nur auf localhost erreichbar
  - CMD um --host=0.0.0.0 erweitern
  
## docker-compose

- Datenbank Passwort falsch

## File permissions

- falsch gesetzt auf lokalem volume ordner
