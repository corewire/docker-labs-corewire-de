# Multistage Build

## Story:

- Demo Repo zeigen
- Einfaches Dockerfile (Buildumgebung + Laufzeit) schreiben
- Dockerfile splitten
- Lösung mit Multistage build

## Demo App zeigen
- Repo klonen: [corewire/hands-on/docker-multistage](https://gitlab.com/corewire/hands-on/docker-multistage)
- Simple Springboot-Applikation, ein Endpunkt unter "/" der "Hello World" ausgibt.
- App.java (Code) und pom.xml (Dependencies) zeigen

## Simples Dockerfile schreiben

```dockerfile
FROM maven:3.8.5-openjdk-18

WORKDIR /multistagedemo

# Install dependencies
COPY pom.xml pom.xml
RUN mvn -f pom.xml dependency:go-offline

# Build jar
COPY ./src ./src
RUN mvn -f pom.xml clean verify -o \
  && mv ./target/dockerdemo-1.0.0.jar dockerdemo.jar

ENTRYPOINT ["java", "-jar", "/multistagedemo/dockerdemo.jar"]
```

```bash
docker build -t multistagedemo:singlestage -f Dockerfile .
docker run -p 8080:8080 multistagedemo:singlestage
# Zugriff auf Webserver zeigen
```

## Separate Dockerfiles - Build stage
Bestehendes Dockerfile für später speichern
```bash
cp Dockerfile Dockerfile.singlestage
```

Dockerfile anpassen
```dockerfile
FROM maven:3.8.5-openjdk-18

WORKDIR /multistagedemo

# Install dependencies
COPY pom.xml pom.xml
RUN mvn -f pom.xml dependency:go-offline

ENTRYPOINT ["mvn", "-f", "pom.xml", "clean", "verify", "-o"]
```

```bash
docker build -t multistagedemo:build -f Dockerfile .
mkdir target
docker run  -v $(pwd):/multistagedemo multistagedemo:build
ls -la target
```

Output ist nun im Target-Ordner

## Separate Dockerfiles - Run stage
Bestehendes Dockerfile für später speichern
```
cp Dockerfile Dockerfile.build
```

Neues Dockerfile als Runtime
```Dockerfile
FROM openjdk:18-slim

WORKDIR /multistagedemo

COPY ./target/dockerdemo-1.0.0.jar dockerdemo.jar

ENTRYPOINT ["java", "-jar", "/multistagedemo/dockerdemo.jar"]
```

```bash
docker build -t multistagedemo:run -f Dockerfile .
docker run -p 8080:8080 multistagedemo:run
# Zugriff auf Webserver zeigen
```

## Multistage Dockerfiles
Bestehendes Dockerfile für später speichern
```
cp Dockerfile Dockerfile.run
```

Neues Dockerfile als Multistage build:
```Dockerfile
## Build stage
FROM maven:3.8.5-openjdk-18 as build

WORKDIR /multistagedemo

# Install dependencies
COPY pom.xml pom.xml
RUN mvn -f pom.xml dependency:go-offline

# Build jar
COPY ./src ./src
RUN mvn -f pom.xml clean verify -o

## Run stage
FROM openjdk:18-slim

WORKDIR /multistagedemo

COPY --from=build /multistagedemo/target/dockerdemo-1.0.0.jar dockerdemo.jar

ENTRYPOINT ["java", "-jar", "/multistagedemo/dockerdemo.jar"]
```


```bash
docker build -t multistagedemo:multistage -f Dockerfile .
docker run -p 8080:8080 multistagedemo:multistage
# Zugriff auf Webserver zeigen
docker image ls multistagedemo
# Größenunterschied zeigen
```