# Einführung

- Hintergrund: Zeigen wann und wie eine Pipeline läuft. Inhalt der Pipeline noch nicht relevant.
- Fokus: Wie ist eine Pipeline im Kontext eines Projektes eingebettet.

## VSCode zeigen
- Wie komme ich drauf?
- Wo ist was?

## Demo-App zeigen

- Nur kurz, weil die immer mal wieder auftauchen wird.

```
docker run -p 8080:5000 corewire/docker-demoapp
```

<http://code-0.{{ domain }}:8080/>

## Gitlab zeigen

- Projekt `gitlab-ci-demoapp` suchen
- Issue erstellen
- MR erstellen
- Datei verändern (z.B. Version in app.py anpassen)
- Pipeline anschauen
- MR anschauen
  - Issue closing Pattern (z.B. `closes` zu `relates` ändern)
  - Tabs: Commits, Pipelines,Changes
  - Approval, Draft
- Issue geschlossen

### Pipeline manuell triggern

### Pipeline per Schedule triggern

`0 /2 * * *` für Pipeline alle 2 Stunden
