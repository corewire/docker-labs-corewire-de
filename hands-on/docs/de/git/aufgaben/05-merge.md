# Merge

!!! important "Ziel"
    - In diesem Projekt geht es um den Umgang mit Branches und wie man Änderungen aus mehreren Branches wieder vereint.

!!! help "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
    [Folien](https://slides.corewire.de/presentations/git/3-branches) und des
    [Cheatsheets](../cheatsheets/3-branches.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1

- Verlassen Sie das Repository `remote-branches` und legen Sie ein neues an:

  ```bash
  cd ..        # Den Order remote-branches verlassen
  mkdir merge  # Einen neuen Ordner anlegen
  cd merge     # In den neuen Ordner wechseln
  git init     # Ein Git-Repository anlegen
  ```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Verlassen Sie den Ordner `remote-branches` mit dem Kommando `cd ..`.
    - Führen Sie `pwd` aus. Sie sollten die Augabe `/root/workspace` erhalten.
    - Sollten Sie die Ausgabe nicht erhalten, wechseln Sie mit `cd /root/workspace` in das Verzeichnis.
    - Wenn Sie im richtigen Verzeichnis sind, führen Sie `mkdir merge` aus.
    - Wechseln Sie mit `cd merge` in den neu angelegten Ordner.
    - Führen Sie `git init` aus. Dadurch wird das Verzeichnis zu einem
      Git-Repository und Sie können Dateien/Änderungen committen.

## Aufgabe 2

- Bauen Sie den untenstehenden Graphen mit folgenden Eigenschaften nach: 
    - Knotenname = Commit-Nachricht
    - Jeder Commit (außer Merge Commits) soll eine Datei mit dem gleichen Namen anlegen.
    - Beispiel: Commit "C1" legt eine neue Datei "C1" an.

![Graph](tikz/project_graph.svg)

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Beginnen Sie mit dem Commit `C1`. Erstellen Sie dafür einen Datei `C1` und
     committen diese mit der Nachricht `C1`.
    - Im Graph sieht man, dass `C1` mehrere Nachfolge-Commits hat. Um später wieder
      zu diesem Commit zu gelangen, ist es ratsam einen temporären Branch
      anzulegen. Legen Sie den Branch mit `git branch branch1` an. Sie befinden
      sich weiterhin auf dem Branch `main`.
    - Legen Sie nun Commit `C2` an.
    - Für Commit `C5` benötigen Sie `C2` und `C3`. Erstellen Sie deshalb erst `C3`.
      Dafür ist der gerade erstellte Branch praktisch. Wechseln Sie auf den Branch
      `branch1`. Sie befinden sich nun wieder bei `C1`. Erstellen Sie den Commit
      `C3`.
    - Wechseln Sie zurück auf `main`. Mit `git merge branch1` können Sie nun
      `C5` erzeugnen. Passen Sie dazu die Commit-Nachricht des Merge-Commits an.
    - Vollenden Sie den Graphen.
