# RDS Monitoring

!!! important "Ziele"
    - Erstellen einer Datenbank mit
      - Erweitertem Logging
      - Enhanced Monitoring
      - Perfomrmance Insights
    - Erstellen eines Cloudwatchalarms

!!! help "Hilfsmittel"
    - Sollten Sie Probleme haben die Aufgaben zu lösen, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.
    - Versuchen Sie die Aufgaben erst einmal ohne die Lösungshilfen zu bearbeiten.

## Aufgabe 1 - Erstellen der Parametergruppen

### DB Parameter Group

Erstellen Sie eine DB Parameter Group und setzten Sie die folgenden Einstellungen:

- `general_log = 1`
- `slow_query_log = 1`
- `log_output = FILE`
- `long_query_time = 1`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Tippen Sie **RDS** in die Suchleiste am oberen Bildschrimrand und wählen Sie den entsprechenden Menüpunkt aus, um auf die RDS Einstellungsseite zu gelangen.
    1. Klicken Sie im Menü am linken Bildschirmrand auf **Parameter groups**
    1. Denken Sie sich einen schönen Namen und eine Beschreibung aus.
    1. Klicken Sie auf die gerade erstellte Parameter Gruppe.
    1. Klicken Sie auf **Edit parameters**.
    1. Suchen Sie im Suchfeld nach den oben aufgeführten Werten und setzten Sie sie entsprechend.
    1. Klicken Sie **Save changes**

### Cluster Parameter Group

Erstellen Sie eine Cluster Parameter Group und setzten Sie die folgenden Einstellungen:

- `server_audit_logging = 1`
- `server_audit_events = CONNECT`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Tippen Sie **RDS** in die Suchleiste am oberen Bildschrimrand und wählen Sie den entsprechenden Menüpunkt aus um auf die RDS Einstellungsseite zu gelangen.
    1. Klicken Sie im Menü am linken Bildschirmrand auf **Parameter groups**
    1. Bei "Type" wählen Sie **DB Cluster Parameter Group**.
    1. Denken Sie sich einen schönen Namen und eine Beschreibung aus.
    1. Klicken Sie auf die gerade erstellte Parameter Gruppe.
    1. Klicken Sie auf **Edit parameters**.
    1. Suchen Sie im Suchfeld nach den oben aufgeführten Werten und setzten Sie sie entsprechend.
    1. Klicken Sie **Save changes**

## Datenbank starten

Starten Sie eine **neue** Aurora Mysql Datenbank mit folgenden zusätzlichen
Einstelungen:

- DB cluster parameter group: Die gerade erstellte
- DB parameter group: Die gerade erstellte
- Enhanced Monitoring mit Granularity 10 seconds
- Log exports: alles aktiviert

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Falls Sie nicht im RDS Bereich sein sollten, navigieren Sie über die Suche dort hin.
    1. Wählen Sie im linken Menü den Eintrag **Datenbanken** aus.
    1. Orangener Button **Datenbank erstellen** anklicken.
    1. Im neuen Fenster diese Einstellungen anwenden:

        ???+ summary "Datenbank Instanz Einstellungen"
            === "Engine options"
                - Type: **Amazon Aurora**
                - Edition: **Amazon Aurora MySQL-Compatible Edition**
                - Capacity Type: **Provisioned**
            === "Settings"
                - DB cluster identifier: **{{ training_name }}-labs-mysql-db**
                - Master username: Sie können `admin` verwenden oder auch anpassen, notieren Sie sich den Usernamen.
                - Master password: Vergeben Sie ein Passwort und notieren Sie es sich.
                ???+ warning "Beachten - WICHTIG!"
                    Das Passwort kann nachträglich nicht mehr geändert werden! Wenn Sie das Passwort vergessen, muss die Instanz neu erstellt werden.

                    Richtlinien für das Passwort: mindestens 8 Zeichen. Nicht erlaubte Zeichen: / (slash), '(single quote), "(double quote) und @ (at Zeichen)
                - Master password confirm: Tragen Sie das Passwort nochmals ein zur Bestätigung.
            === "Availability & durability"
                Multi AZ deployment: **Don't create an Aurora Replica** auswählen.
            === "Connectivity"
                - Virtual private cloud (VPC): **{{ training_name }}-labs-vpc**
                - Subnet Groups: Wählen Sie die Subnetz Gruppe **{{ training_name }}-labs-rds-subnet-group** aus.
                - VPC security groups: Wählen Sie die Security Gruppe **{{ training_name }}-labs-rds-sec-group** aus und entfernen Sie die Gruppe <del>**default**</del>.

    1. Wenden Sie zusätzlich die oben genannten Einstellungen an.

## Beispieldaten anlegen

Benutzen Sie das Script `run_load_data.sh` im VS Code, um die Datenbank mit den Beispieldaten zu befüllen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Öffnen Sie den "File-Explorer" im linken Bereich (unter dem Hauptmenü-Knopf).
    1. Öffnen Sie die Datei `run_load_data.sh` im Ordner `scripts`.
    1. Bearbeiten Sie die **Variables**: `USER=**Ihr DB User**`, `PW=**Ihr DB Passwort**`, `URL=**Url zum DB Endpunkt**`.
    1. Starten Sie ein neues Terminal indem Sie auf den Menü-Knopf (im linken oberen Eck) anklicken und dort **Terminal** :material-arrow-right: **New Terminal** auswählen. Am unteren Bildschirmrand ist nun ein Terminal zugänglich.
    1. Navigieren Sie im Terminal zum `scripts` Pfad mit dem Befehl:
        ```
        cd ~/workspace/scripts
        ```
    1. Führen Sie das Script aus mit dem Befehl:
        ```bash
        ./run_load_data.sh
        ```

## Log Einträge betrachten

Machen Sie sich mit den Logs in Cloudwatch log vertraut und versuchen Sie Logeinträge für verschiedene Logs zu triggern.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Falls Sie nicht im CloudWatch Bereich sein sollten, navigieren Sie über die Suche dort hin.
    1. Wählen Sie im linken Menü den Eintrag **Log groups** aus.
    1. Wählen Sie die entsprechende Log Gruppe.
    1. Einträge im Slow Querry Log können mit einem Select auf die Tabelle `salaries` getriggert werden, z.B.
    ```sql
    Select * FROM salaries where from_date = "2001-11-27";
    ```
    Das funktioniert nur einmal, da die Query dann gecached ist ;)
    1. Einträge im Error Log können mit einem Login mit falschem Passwort getriggert werden.
    1. Einträge im Audit Log können mit belibigem Login getriggert werden.

### Log Einträge durchsuchen

Klicken Sie auf **Log insights** und wählen Sie die Log Gruppe `RDSOSMetrics`. Bauen Sie querries um Ihre Logs zu durchsuchen, die folgende Querry kann ein guter Startpunkt sein:

```
fields @timestamp, @message
| filter cpuUtilization.total > 10
```

## Monitoring einrichten

Erstellen Sie einen Cloudwatchalarm für Ihre Datenbank Instanz mit folgenden Einstellungen:

1. Send notifications to: **New email or SMS topic**
1. With these recipient: Ihre E-Mail Adresse
1. Metric: **Average** of **DB Connections**
1. Threshold: **>=** **10** Count
1. Evaluation period: **1** consecutive period(s) of **1 Minute**

Sie erhalten anschließen eine E-Mail in der Sie die Subscription zu der SNS
Topic bestätigen **müssen** ansonsten werden Sie die anschließende
Benachrichtigung nicht erhalten.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Falls Sie nicht im RDS Bereich sein sollten, navigieren Sie über die Suche dort hin.
    1. Wählen Sie im linken Menü den Eintrag **Databases** aus.
    1. Wählen Sie die Instanz in Ihrem Datenbankcluster aus. Bei Auswahl des
       Cluster selbst lässt sich keine Cloudwatch Alarm einrichten.
    1. Klicken Sie auf den Tab **Logs & events**.
    1. Klicken Sie **Create Alarm**.
    1. Nehmen Sie die oben aufgeführten Einstellungen vor.
    1. Klicken Sie **Create alarm**

## Cloudwatch Alarm testen


1. Passen Sie in der VS Code Console die Variablen in dem  Script
`run_generate_load.sh` an und führen Sie es anschließend aus.
1. Beobachten Sie den Cloud Watch Alarm. Nachdem er auf `Alarm` gewechselt hat,
   sollten Sie eine E-Mail erhalten.

