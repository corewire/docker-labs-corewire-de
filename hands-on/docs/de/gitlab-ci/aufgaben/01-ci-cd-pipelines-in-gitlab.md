# CI/CD-Pipelines in Gitlab

!!! important "Ziel"
    In diesem Projekt geht es um die Verwendung einer bestehenden CI/CD-Pipeline. Wann und wie wird sie ausgeführt? Wie lässt sich eine CI/CD-Pipeline in den Entwicklungsprozess einbinden?

    Sie sollen:

    - Sich mit der Schulungsumgebung vertraut machen.
    - Eine CI/CD-Pipeline über unterschiedliche Wege starten

!!! help "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der [Folien](https://slides.corewire.de/presentations/gitlab-ci/1-introduction/) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Schulungsmaschine einrichten

- Die Schulungsmaschinen sind unter `code-{ZAHL}.{{ domain }}` zu erreichen.
Ersetzen Sie `{ZAHL}` mit der Ihnen zugewiesenen Zahl.
  - Beispiel für die Zahl 5: `code-5.{{ domain }}`.
  - Beispiel für die Zahl 11: `code-11.{{ domain }}`.
- Geben Sie danach das Ihnen zugewiesene Passwort ein.
- Sie haben nun Zugriff auf die Schulungsmaschine.

## Aufgabe 2 - Bei Gitlab anmelden

- Melden Sie sich auf der Gitlab-Instanz für diese Schulung an: [https://git.{{ domain }}/](https://git.{{ domain }}/)
- Username: `user-{ZAHL}`
- Passwort: wird mitgeteilt
- Öffnen Sie das Projekt `gitlab-ci-demoapp`
- Das Projekt enthält die eben gezeigte Demo-Anwendung

## Aufgabe 3 - Issue-MR-Workflow

### 3.1 Issue anlegen

- Eröffnen Sie ein Issue für einen fiktiven Fehler/ ein fehlendes Feature

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Stellen Sie sicher, dass Sie im Projekt `gitlab-ci-demoapp` sind. Oben links im Menü steht das aktuell geöffnete Projekt
    - Klicken Sie links im Menü auf **Issues**
    - Auf dieser Seite können Sie rechts oben über "New Issue" ein Issue anlegen
    - Geben Sie einen Title (z.B. "Bug: xxx funktioniert nicht") ein
    - Weitere Eingaben sind nicht erforderlich
    - Erstellen Sie das Issue unten mit "Create Issue"

### 3.2 Merge Request anlegen

- Das eben erstellte Issue möchten Sie nun bearbeiten/lösen.
- Erstellen Sie aus dem Issue heraus einen Merge Request:
    - (Optional) Passen Sie den Branch-Namen an, indem sie ihm ein Präfix `fix/...` geben
    - Weisen Sie sich den Merge Request zu
    - Erstellen Sie den Merge Request

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Mit dem Erstellen des Issues wurden Sie automatisch auf das Issue weitergeleitet
    - Hier finden Sie den Button "Create Merge Request" mit einem **Pfeil** daneben.
    - Klicken Sie auf den **Pfeil**
    - Hier können Sie den Branch, der erstellt wird, anpassen. Geben Sie `fix/` als Präfix vor dem bestehenden Namen ein.
    - Klicken Sie auf "Create Merge Request"
    - Auf der Seite "New merge request", die sich geöffnet hat, können Sie sich unter "Assignee" den Merge Request zuweisen.
    - Erstellen Sie anschließend den Merge Request

### 3.3 Issue überprüfen

- Öffnen Sie Ihr erstelltes Issue in einem neuen Tab
- Unter "Related merge requests" sollte ihr Merge Request verknüpft sein
- In den Aktivitäten sehen Sie, dass ein Branch angelegt wurde

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - In die Beschreibung des Merge Requests wurde automatisch **Closes #{Issue Nummer}** geschrieben.
    - Per Rechtsklick auf die Issuenummer, können Sie ihr Issue in einem neuen Tab öffnen.
    - Schauen Sie sich die entsprechenden Stellen an. Im Issue wurde genau dokumentiert, dass Sie es bearbeiten und einen Merge Request erstellt haben.
    - Schließen Sie anschließend den Tab wieder.


### 3.4 Source Code bearbeiten

- Gehen Sie zurück zu ihrem Merge Request
- Öffnen Sie ihren Branch in der Web IDE von Gitlab
- Öffnen Sie die Datei `demoapp/app.py`
- Verändern Sie die Version von `2.0.0` auf `2.0.1`
- Erstellen Sie mit ihrer Änderung einen Commit

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Auf dem Merge Request finden Sie oben rechts den Button **Code**
    - Klicken Sie ihn und wählen Sie "Open in Web IDE"
    - Es öffnet sich der in Gitlab integrierte Text-Editor
    - Suchen Sie links im Dateibrowser den Ordner **demoapp** und darin die Datei **app.py**
    - Verändern Sie die Versionsnummer in Zeile 13 von `2.0.0` zu `2.0.1`.
    - Unterhalb des Dateibrowsers auf der linken Seite wird dadurch der Button "Create commit..." aktiviert
    - Klicken Sie diesen Button und commiten Sie ihre Änderungen auf ihren Branch `fix/...`

### 3.5 Pipeline-Logs ansehen

- Verlassen Sie die Gitlab Web IDE und gehen Sie zurück zu ihrem Merge Request
- Schauen Sie im Tab "Pipelines" nach der von ihrem Commit getriggerten Pipeline
- Schauen Sie sich das Job-Log an

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Verlassen Sie die Gitlab Web IDE indem Sie oben links auf den Projektnamen **Gitlab Ci Demoapp** klicken
    - Öffnen Sie ihren Merge Request links über das Menü **Merge requests**
    - Gehen Sie im Merge Request auf den Tab Pipelines (direkt unter dem Titel)
    - Hier sehen Sie die zu diesem Merge Request gehörenden Pipelines
    - Schauen Sie sich den Job im Detail mit Klick auf den **grünen Kreis** unter **Stages** an.
    - Hier sehen Sie die Logausgabe des Jobs sowie weitere Informationen.


## Aufgabe 4 - Pipeline manuell triggern

- Navigieren Sie links im Menü zu CI/CD ⇨ Pipelines
- Hier finden Sie alle Pipelineausführungen für ihr Projekt
- Triggern Sie eine manuelle Pipeline
- Schauen Sie sich in der Pipeline-Ansicht etwas um

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Gehen Sie auf die Pipeline-Übersicht im Menü über CI/CD ⇨ Pipelines
    - Rechts oben befindet sich der Button **Run pipeline**. Klicken Sie diesen und bestätigen Sie die folgende Seite ebenfalls mit "Run pipeline"

## Aufgabe 5 - Scheduled Pipeline anlegen

- Navigieren Sie links im Menü zu CI/CD ⇨ Schedules
- Hier können Sie geplante, sich wiederholende Pipelines anlegen
- Erstellen Sie einen Schedule
