# Rebase

!!! important "Ziel"
    - In diesem Projekt sollen Sie sich mit dem Rebase, der Alternative zum Mergen, vertraut machen.

!!! help "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
     [Folien](https://slides.corewire.de/presentations/git/4-branches-2) und des
     [Cheatsheets](../cheatsheets/3-branches.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1

Verlassen Sie das Repository `conflict` und legen Sie ein neues an:

```bash
cd ..         # Den Order conflict verlassen
mkdir rebase  # Einen neuen Ordner anlegen
cd rebase     # In den neuen Ordner wechseln
git init      # Ein Git-Repository anlegen
```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Verlassen Sie den Ordner `conflict` mit dem Kommando `cd ..`.
    - Führen Sie `pwd` aus. Sie sollten die Augabe `/root/workspace` erhalten.
    - Sollten Sie die Ausgabe nicht erhalten, wechseln Sie mit `cd /root/workspace` in das Verzeichnis.
    - Wenn Sie im richtigen Verzeichnis sind, führen Sie `mkdir rebase` aus.
    - Wechseln Sie mit `cd rebase` in den neu angelegten Ordner.
    - Führen Sie `git init` aus. Dadurch wird das Verzeichnis zu einem
      Git-Repository und Sie können Dateien/Änderungen committen.

## Aufgabe 2

- Bauen Sie den unten stehenden Graphen mit folgenden Eigenschaften nach: 
    - Knotenname = Commit-Nachricht
    - Jeder Commit (außer Merge Commits) soll eine Datei mit dem gleichen Namen anlegen.
    - Beispiel: Commit `C1` legt eine neue Datei `C1` an.

![Graph](tikz/rebase_graph.svg)

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Erstellen Sie zuerst den Branch `us-42` mit `git switch -c us-42`.
    - Erstellen Sie Commit `C1` indem Sie eine Datei `C1` anlegen und diese committen.
    - Machen Sie dasselbe für `C2`.
    - Erstellen Sie den Branch `testing` mit `git switch -c testing`.
    - Erstellen Sie Commit `C3`.
    - Wechseln Sie zurück auf `us-42` mit `git switch us-42`.
    - Erstellen Sie Commit `C4`.

## Aufgabe 3

Führen Sie einen Rebase aus, sodass die Änderungen von `C3` auf `C4`
 angewendet werden und der Branch `us-42` auf den neuen Commit `C3` zeigt.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Sie haben nun einen Branch `us-42` und einen Branch `testing`. Der
      `testing`-Branch beinhaltet leider nicht die neusten Änderungen von `us-42`.
      Das wollen wir nun ändern, indem wir einen Rebase ausführen.
    - Wechslen Sie dazu auf den Branch `testing` mit `git switch testing`.
    - Starten Sie den Rebase mit `git rebase us-42` damit die Änderungen von `testing`
      nun wieder auf den aktuellen Änderungen von `us-42` basieren.
    - Als Letztes wollen wir nun die Änderungen von `testing` in `us-42`
      integrieren. Wechseln Sie dazu mit `git switch us-42` auf den Branch `us-42`.
    - Führen Sie mit `git merge testing` einen Fast-Forward-Merge aus.
    - Sehen Sie sich mit `git log` das Ergebnis an.
