# Cheatsheet 4 - History Rewriting

| Command                                                          | Action                                                  |
|------------------------------------------------------------------|---------------------------------------------------------|
| ```git rebase -i HEAD~4```                                       | Interaktiver Rebase der letzten 4 Commits               |
| ```git cherry-pick <commit-hash> [...more commit hashes...]```   | Einzelnen Commit auf HEAD anwenden                      |
|                                                                  |                                                         |
| ```git apply <patchfile>```                                      | Patch-Datei anwenden                                    |
| ```git format-patch <branch> -o <dir>```                         | Unterschiede des aktuellen Branch zu `<branch>` als Patch-Dateien in `<dir>` generieren |
| ```git format-patch <branch> -1 <commit_hash> -o <dir>```        | Generiert Patch-Datei für `<commit_hash>` in `<dir>`    |
| ```git am <patchfiles>```                                        | Wendet Patch-Dateien an und erstellt Commits daraus     |
|                                                                  |                                                         |
| ```git filter-branch --tree-filter <commmand> HEAD```            | Alle Parent-Commits auschecken und Command anwenden     |
| ```git filter-branch --all --tree-filter <command>```            | Alle Commits auschecken und Command anwenden            |
| ```git filter-branch --index-filter <command> HEAD```            | Command auf allen Parent-Commits im INDEX ausfügren     |
|                                                                  |                                                         |
| ```git push --force-with-lease```                                | Remote Versions DB mit lokaler überschreiben            |
|                                                                  |                                                         |
| ```git reflog```                                                 | Zeigt das Reflog für HEAD                               |
| ```git reflog show <branch_name>```                              | Zeigt das Reflog für `<branch_name>`                    |
| ```git reflog stash```                                           | Zeigt das Reflog für den Stash                          |
