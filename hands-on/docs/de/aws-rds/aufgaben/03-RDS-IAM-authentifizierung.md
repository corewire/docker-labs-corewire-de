# RDS MySQL Instanz erstellen

!!! important "Ziele"
    - Authentifizierung an der Datenbank mit dynamischen Credentials aus einer
      IAM Rolle

!!! help "Hilfsmittel"
    - Sollten Sie Probleme haben die Aufgaben zu lösen, finden Sie bei jeder
      Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben
      wird.
    - Versuchen Sie die Aufgaben erst einmal ohne die Lösungshilfen zu bearbeiten.

## Aufgabe 1 - IAM Authentifizierung aktivieren

Erstellen Sie eine neue Datenbank oder nutzen Sie eine aktuell laufende. Für
die Datenbank sollte "Password and IAM database authentication" aktiviert sein.


!!! warning "Achtung"
    
    Die Datenbankinstanz muss mindestens die Größe `db.r5.large` haben, da
    sonst die Funkton "Password and IAM database authentication" nicht
    verfügbar ist.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Falls Sie nicht im RDS Bereich sein sollten, navigieren Sie über die Suche dort hin. 
    1. Wählen Sie im linken Menü den Eintrag **Datenbanken** aus.
    1. Orangener Button **Datenbank erstellen** anklicken.
    1. Im neuen Fenster diese Einstellungen anwenden:
        
        ???+ summary "Datenbank Instanz Einstellungen"
            === "Engine options"
                - Type: **Amazon Aurora**
                - Edition: **Amazon Aurora MySQL-Compatible Edition**
                - Capacity Type: **Provisioned**
            === "Settings"
                - DB cluster identifier: **{{ training_name }}-labs-mysql-db**
                - Master username: Sie können `admin` verwenden oder auch anpassen, notieren Sie sich den Usernamen.
                - Master password: Vergeben Sie ein Passwort und notieren Sie es sich.
                ???+ warning "Beachten - WICHTIG!"
                    Das Passwort kann nachträglich nicht mehr geändert werden! Wenn Sie das Passwort vergessen, muss die Instanz neu erstellt werden.

                    Richtlinien für das Passwort: mindestens 8 Zeichen. Nicht erlaubte Zeichen: / (slash), '(single quote), "(double quote) und @ (at Zeichen)
                - Master password confirm: Tragen Sie das Passwort nochmals ein zur Bestätigung.
            === "Availability & durability"
                Multi AZ deployment: **Don't create an Aurora Replica** auswählen.
            === "Connectivity"
                - Virtual private cloud (VPC): **{{ training_name }}-labs-vpc**
                - Subnet Groups: Wählen Sie die Subnetz Gruppe **{{ training_name }}-labs-rds-subnet-group** aus.
                - VPC security groups: Wählen Sie die Security Gruppe **{{ training_name }}-labs-rds-sec-group** aus und entfernen Sie die Gruppe <del>**default**</del>.
            === "Database authentication"
                - Wählen Sie: **Password and IAM database authentication**

    1. Überprüfen Sie nochmals ihre Angaben und bestätigen Sie das Erstellen der Instanz mit einem Klick auf: **create database**

## Aufgabe 2 - IAM Rolle für die EC2 Instanz

Damit die EC2 Instanz Zugriff auf die Datenbank erhält, muss dieser die
entsprechende Rolle zugewiesen werden. Wir erstellen dazu zuerst die Policy und
mit dieser anschließend die Rolle.

### IAM Policy Erstellen

Erstellen Sie eine Policy, die die Authentifizierung via IAM für den Datenbankcluster erlaubt. 

!!! warning "Achtung"
    
    Der in der IAM Policy referenzierte Nutzer darf nicht der bisher schon
    vorhandene Datenbank-Administrator sein.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Tippen Sie **IAM** in die Suchleiste am oberen Bildschrimrand und wählen
       Sie den entsprechenden Menüpunkt aus, um auf die IAM Einstellungsseite zu
       gelangen.
    1. Klicken Sie am linken Bildschirmrand auf **Policies**.
    1. Klicken Sie auf **Create Policy**.
    1. Klicken Sie auf **Choose a service**.
    1. Suchen Sie nach **RDS**.
    1. Wählen Sie **RDS IAM Authentication**.
    1. Klappen Sie unter "Access level" das **Permission management** aus.
    1. Wählen Sie **connect**.
    1. Klappen Sie "Resources" aus.
    1. Klicken Sie **Add ARN**.
    1. Bei region tragen Sie `eu-central-1` ein.
    1. Die `Db resource id` finden Sie im Datenbankcluster im Reiter `Configuration`.
    1. Bei `Db user name` tragen Sie einen Namen ein und merken Sie ihn sich.
       Er sollte nicht dem DB Admin user entsprechen.
    1. Klicken Sie auf **Next: Tags**
    1. Klicken Sie auf **Next: Review**
    1. Vergeben Sie einen tollen Namen :)


### IAM Rolle Erstellen

Erstellen Sie eine Rolle für den EC2 Zugriff auf RDS mit der zuvor konfigurierten Policy.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Tippen Sie **IAM** in die Suchleiste am oberen Bildschrimrand und wählen
       Sie den entsprechenden Menüpunkt aus, um auf die IAM Einstellungsseite zu
       gelangen.
    1. Klicken Sie am linken Bildschirmrand auf **Roles**.
    1. Klicken Sie auf **Create role**.
    1. Wählen Sie bei "Trusted entity type" **AWS service** aus.
    1. Bei "Use case" wählen Sie **EC2** aus.
    1. Klicken Sie **Next**.
    1. Suchen Sie nach ihrer zuvor erstellten Policy und wählen Sie sie aus.
    1. Klicken Sie **Next**.
    1. Geben Sie einen tollen Namen ein :)
    1. Klicken Sie **Create role**.

### IAM Rolle an EC2 Instanz anfügen

Fügen Sie die gerade erstelle IAM Rolle an die laufende EC2 Instanz **{{ training_name }}-labs-vscode-instance** an.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Tippen Sie **EC2** in die Suchleiste am oberen Bildschrimrand und wählen
       Sie den entsprechenden Menüpunkt aus, um auf die EC2 Einstellungsseite zu
       gelangen.
    1. Klicken Sie im Menü am linken Bildschirmrand auf **Instances**.
    1. Wählen Sie die Instanz **{{ training_name }}-labs-vscode-instance** aus.
    1. Klicken Sie auf **Actions** :material-arrow-right: **Security** :material-arrow-right: **Modify IAM role**.
    1. Wählen Sie die gerade erstellte Rolle aus.
    1. Klicken Sie **save**.

## IAM User in der Datenbank hinterlegen

Verbinden Sie sich auf der VisualStudio Code Instanz zur Datenbank mit dem
Datenbank Admin User und führen Sie folgendes SQL Statement aus:

```sql
CREATE USER <iam user> IDENTIFIED WITH AWSAuthenticationPlugin AS 'RDS';
```

Wobei `iam user` der in der IAM Policy spezifizierte Nutzer ist.


## Datenbankzugriff per IAM Rolle

Da die IAM Authentifizierung zwingend per SSL erfolgen muss, brauchen wir noch
das entsprechende Zertifikat. Laden Sie es mit folgemdem Befehl herunter:

```shell
curl https://truststore.pki.rds.amazonaws.com/ca-central-1/ca-central-1-bundle.pem > ca-central-1-bundle.pem
```

Wir können uns nun mit der IAM Rolle zu Datenbank verbinden:

1. Passen Sie im Skript `run_rds_iam_auth.sh` die Variablen an.
1. Führen Sie das Skipt aus.

Sie sind jetzt mit der Datenbank per IAM Authentifizierung verbunden :rocket:


