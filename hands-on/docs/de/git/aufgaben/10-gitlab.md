# Gitlab - Issues und Merge Requests

!!! important "Ziel"
    - In diesem Projekt werden wir durchspielen, wie die Zusammenarbeit im Team mit
    Gitlab ablaufen kann.
    - Die erlernten Techniken sind dabei auf andere Services
    wie Github oder Bitbucket übertragbar.
    - Ziel ist es, ein neues Ticket für einen
    Bug anzulegen und den Bug anschließend in einem Merge Request selbst zu
    beheben und damit zu schließen.

## Aufgabe  1

Schauen Sie sich das Repository `api-users` in Gitlab an. Es enthält einen API
Server zur Verwaltung von Nutzern. Leider enthält es keinerlei Dokumentation.
Erstellen Sie einen neuen Issue zum Erstellen einer Dokumentation. Im
Beschreibungsfeld können Sie dabei Markdown verwenden. Starten Sie mit dem
folgenden Beispiel und probieren Sie verschiedene Markdown Funktionen aus. Mit
einem Klick auf `Preview` können Sie das gerenderte Markdown anschauen.

Die folgenden Punkte sollten dokumentiert werden:

- Was das Ziel dieser API ist.
- Wie die API verwendet wird.
- Wie sich der Server aufsetzen lässt.

In einem solchen Issue lässt sich nun im Team diskutieren, wie man zu einer
Lösung kommt. Im folgenden werden wir einen Merge Request erstellen und darin
eine Lösung implementieren.

## Aufgabe  2

Erstellen Sie den Merge Request mit einem Klick auf `Create merge request`. Der
Merge Request wird jetzt in dem zuvor erstellten Issue angezeigt und sollte auch direkt geöffnet werden.

Wir müssen diesen jetzt mit der entsprechenden Änderung befüllen. Für den Merge
Request wurde ein neuer Branch erstellt.

- Wechseln Sie zu Vistual Studio Code und klonen Sie das Repository.
- Wechseln Sie auf den Branch des Merge Requests.
- Fügen Sie eine `README.md` hinzu mit einem kurzen Beschreibungstext. Hierbei
  können Sie wieder Markdown verwenden.
- Comitten und pushen Sie Ihre Änderungen auf den Server.

Wenn Sie nun zurück in den Merge Request wechseln sehen Sie, dass der neue
Commit im Merge Request angezeigt wird. Es wurde außerdem eine Pipeline
getriggert. Diese prüft, dass auch nach der Änderung alle Tests und Linter
durchlaufen. Das ist in unserem Fall nicht wichtig, da keine Änderungen am
Programmcode durchgeführt wurden. Ansonsten gibt es ein wichtiges Feedback
darüber, ob alles wie vorgesehen funktioniert. Wenn wir mit den Änderungen
zufrieden sind können wir mit einem Klick auf `Resolve WIP status` dem Rest
des Teams signalisieren, dass die Änderungen auf `master` übernommen werden
können.

- Klicken Sie `Resolve WIP status`.

Im Anschluss müssen wir die Änderungen noch auf `main` übernehmen. Das wird im
Normalfall durch einen Kollegen gemacht, der die von uns vorgenommenen Änderungen
gegenliest und so ein Codereview durchführt.

- Klicken Sie auf `Merge when pipeline succeeds` um den Merge Request zu
  schließen oder auf `Close merge request`, wenn die Pipeline bereits gelaufen ist.

Ist die Pipeline abgeschlossen wird auch der Merge Request geschlossen und
damit wird dann auch automatisch der Issue geschlossen.

- Gehen Sie zurück zum Issue, der jetzt unter `Closed` zu finden ist.

Damit ist der gesammte Workflow für einen Issue, von Anfang bis Ende, abgeschlossen.
