# Cheatsheet 4.1 - Docker-compose Befehle

Services werden in der `docker-compose.yml` definiert. Diese muss sich im aktuellen
Verzeichnis befinden. `<Service...>` kann durch einen oder mehrere Services ersetzte werden. Man kann
es auch komplett weg lassen, dann wird der Befehl auf alle Services angewendet.

| Befehl                                              | Aktion                                                            |
|-----------------------------------------------------|-------------------------------------------------------------------|
| ```docker-compose up -d <Service...>```             | Einen oder mehrere Services erstellen und starten                 |
| ```docker-compose down```                           | Alle Services stoppen und löschen                                 |
| ```docker-compose build <Service...>```             | Einen oder mehrere Services bauen                                 |
| ```docker-compose pull <Service...>```              | Die Images von einem oder mehreren Services pullen                |
| ```docker-compose logs <Service...>```              | Die Logs von einem oder mehreren Services ausgeben                |
| ```docker-compose ps```                             | Die laufenden Container anzeigen lassen                           |
| ```docker-compose top <Service...>```               | Die laufenden Prozesse von einem oder mehreren Services auflisten |
| ```docker-compose create <Service...>```            | Einen oder mehrere Services erstellen                             |
| ```docker-compose start <Service...>```             | Einen oder mehrere Services starten                               |
| ```docker-compose stop <Service...>```              | Einen oder mehrere Services stoppen                               |
| ```docker-compose rm <Service...>```                | Einen oder mehrere Services löschen                               |
| ```docker-compose exec <Service> <Befehl>```        | Befehl in Container mit TTY ausführen                             |
| ```docker-compose exec -T <Service> <Befehl>```     | Befehl in Container ohne TTY ausführen                            |


# Cheatsheet 4.2 - docker-compose.yml

Alle Einträge, die mit `cw-` beginnen, sind frei wählbare Namen. Alle Zeilen die
mit `#` anfangen, sind Kommentare.

## Struktur

```yml
version: '3.7'
services:
  # Einen oder mehrere Services anlegen
  cw-webapp:
    # build and image are mutually exclusive.
    # build points to a directory containing the Dockerfile. This
    # file will be build
    build: ./path/to/directory
    # define the image with optional tag. Will be pulled if not present
    image: <image>:<tag>

    # List of dependend services
    ## This service (cw-webapp) will be started after all services it
    ## depends on have started
    depends_on:
      - cw-database

    # List of port forwarding entries
    ports:
      -  5000:5000

    # List of volumes
    volumes:
      - cw-webapp-volume:/app/data/notes

    # List of networks
    networks:
      - cw-database-network

    # List of environment variables
    environment:
      - "MYSQL_USER=example-user"
      - "MYSQL_PASSWORD=..."

  # Zweiter Service:
  cw-database:
    [...]

# (Optional) Ein oder mehrere Managed Volumes anlegen
volumes:
  cw-webapp-volume:

# (Optional) Ein oder mehrere Netzwerke anlegen
networks:
  cw-database-network:
```
