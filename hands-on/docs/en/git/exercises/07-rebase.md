# Rebase

!!! important "Goal"
    - This project is about rebase. Rebasing is an alternative to merging.

!!! help "Hints"
    - Try to solve the following tasks with the help of the [Slides](https://slides.corewire.de/presentations/git/4-branches-2)
    and the [Cheatsheets](../cheatsheets/3-branches.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1

- Leave the repository `conflict` and create a new one:

   ```bash
   cd ..           # Leave the directory conflict
   mkdir rebase    # Create a new directory rebase
   cd rebase       # Change into that directory
   git init        # Create a new Git repository
   ```

??? help "Solution (Click on the arrow if you are stuck)"
    - Leave the directory `conflict` with the command `cd ..`.
    - Run the command `pwd` and you should see `/root/workspace` as a result.
    - If you got a different result, run `cd /root/workspace` to change into the right directory.
    - When you are in the right directory, run `mkdir rebase`.
    - Change into the newly created directory with `cd rebase`.
    - Run `git init` which creates a new git repository in the current directory.


## Task 2

- Create the following graph. Each commit/node should have the following properties:
    - Node name = commit message
    - Each commit (except merge commits) should create a file with the name of the commit message.
    - Example: commit `C1` creates a new file `C1`.

![Graph](tikz/rebase_graph.svg)

??? help "Solution (Click on the arrow if you are stuck)"
    - Create the branch `us-42` with `git switch -c us-42`.
    - Create commit `C1` by creating and commiting a file `C1`.
    - Repeat the same for `C2`.
    - Create the branch `testing` with `git switch -c testing`.
    - Create commit `C3`.
    - Switch back to `us-42` with `git switch us-42`.
    - Create commit `C4`.

## Task 3

Do a rebase, such that the changes of `C3` are applied to `C4`. The branch `us-42` should point on the new `C3` commit.

??? help "Solution (Click on the arrow if you are stuck)"
    - You should now have a branch `us-42` and a branch `testing`.
    - The branch `testing` does not include the latest changes of `us-42`.
    - We want to change this by doing a rebase.
    - For that, switch to the branch `testing` with `git switch testing`.
    - Begin the rebase with `git rebase us-42`. Now, the changes of `testing` include all other changes of `us-42`.
    - As a last step, we want to integrate the changes of `testing` into `us-42`.
    - Switch to the branch `us-42` with `git switch us-42`.
    - Execute a fast forward merge with `git merge testing`.
    - Review the result with `git log`.
