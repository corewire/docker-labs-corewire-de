# Einführung ins Projektmanagement mit Gitlab

- [Zu den Aufgaben](./aufgaben/01-ticket-lifecycle.md)

## Dauer
0,5 Tage

## Zielgruppe
- Softwareentwickler:innen
- Product Owner
- Supportmitarbeiter:innen
- UI/UX-Engineer

## Voraussetzungen
Keine

## Kursziel
Der Kurs vermittelt den Umgang mit Tickets/Issues, Mergerequest und Projektmanagement-Boards in Gitlab.

## Schulungsform
Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **60% theoretischen Inhalten** und **40% praktischen Übungen**.
