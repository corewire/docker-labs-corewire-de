# Sync Deployment

## 3.1 YAML-Features

Im Pipeline-Editor, switch beween merged view and edit view:

```yml
job_1:
  image: python:3.9
  script:
    - echo Running job 1...

job_2:
  image: python:3.9
  script:
    - echo Running job 2...
```
Hidden Job, ignored by CI:
```yml
.hidden_job:
  image: python:3.9

job_1:
  image: python:3.9
  script:
    - echo Running job 1...

job_2:
  image: python:3.9
  script:
    - echo Running job 2...
```
Add anchor:
```yml
.hidden_job: &base_job
...
```

Add map merging to jobs
```yml
...

job_1:
  <<: *base_job
  script:
    - echo Running job 1...

job_2:
  <<: *base_job
  script:
    - echo Running job 2...
```

Add `before_script` to hidden job.

## 3.2 SSH Zugriff auf VSCode

- Code aus Folien kopieren:

```
  script:
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y &&
        apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
```

- Code zu valider `.gitlab-ci.yml` erweitern

```
stages:
  - deploy

deploy:
  stage: deploy
  script:
    - ...
```

- SSH_PRIVATE_KEY und SSH_KNOWN_HOST setzen

Für Known Host
```
ssh-keyscan code-0.{{ domain }}
```

- SSH Zugriff testen:

```
- ssh root@code-0.{{ domain }} "touch /home/coder/workspace/hello.world"
```

- Ganzes Beispiel

```
stages:
  - deploy

deploy:
  stage: deploy
  script:
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y &&
        apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - ssh root@code-0.{{ domain }} "touch /home/coder/workspace/hello.world"
```

## 3.3 Container auf VS-Code starten

```
stages:
  - deploy
  - stop_review

deploy_review:
  stage: deploy
  before_script:
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - >
      echo "
      set -e

      docker run -d \
       --name review_$CI_COMMIT_REF_SLUG \
       --network proxy \
       --label traefik.enable=true \
       --label traefik.http.services.review_$CI_COMMIT_REF_SLUG.loadbalancer.server.port=80 \
       --label 'traefik.http.routers.review_$CI_COMMIT_REF_SLUG.rule=Host(\`$CI_COMMIT_REF_SLUG.code-0.{{ domain }}\`)' \
       nginx

      docker exec review_$CI_COMMIT_REF_SLUG bash -c \"echo \\\"Review App for $CI_COMMIT_REF_SLUG\\\" > /usr/share/nginx/html/index.html\"
      " >> deploy.sh
    - cat deploy.sh
    - ssh root@code-0.{{ domain }} "bash -s" < deploy.sh
```

## 3.4 Review App einrichten

- In Gitlab, Deployments, Envorinments "Enable review app"
- Code kopieren
- Deploy Review wie folgt erweitern
```
deploy_review:
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_COMMIT_REF_SLUG.code-0.{{ domain }}
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script:
    - ...
```


## 3.5 Stop Review App

- Deploy Review Environment erweitern mit

```
    on_stop: stop_review_app
```

- stop_review_app anlegen

```
stop_review_app:
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
  script:
    - ssh root@code-0.{{ domain }} "docker stop review_$CI_COMMIT_REF_SLUG && docker rm review_$CI_COMMIT_REF_SLUG"
```

- Extract config ssh

```
.configure-ssh: &configure-ssh
  - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
```

- Script erweitern

```
  script:
    - *configure-ssh
    - ssh root@code-0.{{ domain }} "docker stop review_$CI_COMMIT_REF_SLUG && docker rm review_$CI_COMMIT_REF_SLUG"
```

## 3.5 Fertiges Beispiel

```
stages:
  - deploy
  - stop_review


.configure-ssh: &configure-ssh
  - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )'
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts

deploy_review:
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_NAME
    on_stop: stop_review_app
    url: https://$CI_COMMIT_REF_SLUG.code-0.{{ domain }}
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script:
    - *configure-ssh
    - >
      echo "
      set -e

      docker run -d \
       --name review_$CI_COMMIT_REF_SLUG \
       --network proxy \
       --label traefik.enable=true \
       --label traefik.http.services.review_$CI_COMMIT_REF_SLUG.loadbalancer.server.port=80 \
       --label 'traefik.http.routers.review_$CI_COMMIT_REF_SLUG.rule=Host(\`$CI_COMMIT_REF_SLUG.code-0.{{ domain }}\`)' \
       nginx

      docker exec review_$CI_COMMIT_REF_SLUG bash -c \"echo \\\"Review App for $CI_COMMIT_REF_SLUG\\\" > /usr/share/nginx/html/index.html\"
      " >> deploy.sh
    - cat deploy.sh
    - ssh root@code-0.{{ domain }} "bash -s" < deploy.sh

stop_review_app:
  stage: stop_review
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
  script:
    - *configure-ssh
    - ssh root@code-0.{{ domain }} "docker stop review_$CI_COMMIT_REF_SLUG && docker rm review_$CI_COMMIT_REF_SLUG"
```
