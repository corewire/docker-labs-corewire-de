# Git LFS & Bisect

!!! important "Ziel"
    - In diesem Projekt werden wir den Umgang mit Git-LFS lernen und dafür eine Webseite in git versionieren.
    - Dazu wird das Thema Git Bisect behandelt.

## Aufgabe  1

Zunächst müssen wir ein neues Repository anlegen.

- Legen Sie in Vistual Studio Code einen neuen Ordner `Webseite` an.
- Wechseln Sie in den Ordner und erstellen Sie hier ein neues Respository mit
  `git init`

- Nun brauchen wir eine Webseite. Auf der Seite <https://freehtml5.co/> finden
sich zahlreiche Vorlagen für Webseiten. Suchen Sie sich ein Template aus, das
Ihnen gefällt.

- Klicken Sie mit rechts auf den Download Button und kopieren Sie den Download Link.
- Laden Sie die Website als `zip` herunter mit:

```bash
curl --output website.zip <URL>
```

- Entpacken Sie die Webseite mit:

```bash
unzip website.zip
```

- Löschen Sie `website.zip`.

## Aufgabe  2

Wir haben nun alle Dateien der Webseite in unserem Repository und wollen sie jetzt comitten.
Dabei wollen wir die Binärdateien wie Bilder in git LFS hinterlegen.
Zunächst müssen wir git mitteilen, dass wir git LFS in diesem
Repository verwenden wollen:

```bash
git lfs install
```

Für alle Binärdateien müssen wir git nun mitteilen, dass wir diese in LFS
managen wollen. Das können wir für beliebige Dateiendungen konfigurieren mit:

```bash
git lfs track "-.<Dateiendung>"
```

Wichtig ist hier, dass die Anführungszeichen nicht vergessen werden. Welche
Dateiendungen wir tracken müssen hängt in diesem Fall davon ab, welche Dateien
es in userer Webseite gibt. Das Ergebniss der track Befehle können wir in der
Datei `.gitattributes` betrachten. Nun können wir committen:

- Committen Sie die Webseite.

## Aufgabe  3

Im folgenden werden wir unser Repository nach Gitlab pushen. Dort finden
Sie ein Repository mit dem Namen `git-lfs`. Auf dessen Startseite ist
beschrieben, wie Sie ihr bestehendes Repository hierhin pushen können.

- Pushen Sie das Repository.

Beachten Sie die Ausgabe zur Verarbeitung der Git LFS Dateien. In Gitlab können
Sie die entsprechenden Dateien ebenfalls anschauen und sehen dann, dass diese im
Hintergrund mit LFS verwaltet werden. Wenn ein anderer Benutzer dieses Repository nun
bei sich auscheckt und `git lfs` installiert hat, wird alles automatisch
eingerichtet und funktioniert wie gewohnt.

## Aufgabe 4 - Bisect Vorbereitungen

Suchen Sie in Gitlab das Projekt `bisect` und klonen Sie es.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Suchen Sie in Gitlab links oben unter `Menu->Projects` nach dem Projekt `bisect`.
    Wenn Sie das Projekt hier nicht direkt sehen, finden Sie es unter `Your projects`.

    - Kopieren Sie den Link unter `Clone with HTTPS`.
    - Gehen Sie zurück ins Terminal in der VS Code Instanz.
    - Stellen Sie sicher, dass Sie sich in `/root/workspace` befinden. Verlassen
    Sie etwaige vorherige Repositories mit `cd ..` (alternativ können Sie mit
    `cd /root/workspace` direkt in das Verzeichnis wechseln).
    - Klonen Sie das Projekt mit `git clone {URL}`.

## Aufgabe 5 - Manueller Bisect

Ein befreundeter Entwickler bittet Sie um Rat. Er hat bemerkt, dass etwas
auf seinem Branch kaputt ist aber er findet den Commit nicht, ab dem der Fehler auftritt.
Alles was Sie wissen ist:

- Auf `main` funktioniert es.
- Auf seinem Branch `bisect_manual` funktioniert es nicht.
- Der Branch `bisect_manual` ist 50 Commits vor `main`.

Der Einfachheit halber enthält der Branch nur eine Datei mit dem Namen `test.txt`.
Die Datei enthält entweder den Text `Here is my good code...` oder `Here is my bad code...`.
Ihre Aufgabe ist es, den Commit zu finden, der die Änderung von `good` zu `bad` verursacht hat.

- Starten Sie, indem sie zum Branch `bisect_manual` switchen.
- Sehen Sie sich die Datei `test.txt` (`cat test.txt`) an.
- Beginnen Sie die Suche mit `git bisect start`.
- Markieren Sie den letzten bekannten guten Commit mit `git bisect good <id>` und den
  aktuellen bösen Commit mit `git bisect bad <id>`.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Um den Bisect-Prozess zu starten, benutzen Sie folgende Befehle:
        ```bash
        git bisect start
        git bisect good main
        git bisect bad HEAD
        ```
    - Git checkt einen Commit mittig zwischen `main` und HEAD aus. Prüfen Sie,
    ob dieser Commit gut oder böse ist (`cat test.txt`) und markieren Sie den Commit
    dementsprechend mit `git bisect good` oder `git bisect bad`.

    - Wiederholen Sie diesen Prozess, bis Sie zu dem Commit kommen, der den Fehler im Code verursacht hat.

Sobald Sie fertig sind, prüfen Sie die Änderungen des gefundenen Commits und beenden Sie den Bisect-Prozess.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Prüfen Sie die Änderungen im Commit mit `git show <id>`.
    - Den Bisect-Prozess beenden Sie mit `git bisect reset`.


## Aufgabe 6 - Automatisierter Bisect

Fanden Sie es lästig, jeden Commit von Hand zu überprüfen?

Bisect bietet eine run-Option an, die dabei hilft, den Prozess zu automatisieren.
Das kann sehr hilfreich sein, wenn Sie einen Commit in einem größeren Teil Ihrer
Projekthistorie finden möchten.
Dieses Mal hat der Branch 50.000 Commits und zusätzlich zur `test.txt` gibt es ein
`check_commit.sh` Skript, dessen Zweck es ist, zwischen guten und bösen Commits zu unterscheiden.

- Starten Sie, indem Sie zum Branch `bisect_auto` switchen.
- Sehen Sie sich den Branch an.
- Sehen Sie sich das Skript an.
- Starten Sie die Suche mit `git bisect start`.
- Nachdem Sie den guten und bösen Commit markiert haben, probieren Sie `git bisect run...` aus.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Den Bisect-Prozess starten Sie mit:
        ```bash
        git bisect start
        git bisect good main
        git bisect bad HEAD
        ```
    - Um die Automatisierung mit dem Skripts zu starten, geben Sie folgenden Code ein:
        ```bash
        git bisect run ./check_commit.sh
        ```
