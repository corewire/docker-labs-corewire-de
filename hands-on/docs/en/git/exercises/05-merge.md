# Merge

!!! important "Goal"
    - This project is about working with branches and how to merge changes of multiple branches into one again.

!!! help "Hints"
    - Try to solve the following tasks with the help of the [Slides](https://slides.corewire.de/presentations/git/3-branches)
    and the [Cheatsheets](../cheatsheets/3-branches.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1

- Leave the repository `remote-branches` and create a new one:

  ```bash
  cd ..        # Leave the directory remote-branches
  mkdir merge  # Create a new directory merge
  cd merge     # Change into that directory
  git init     # Create a new Git repository
  ```

??? help "Solution (Click on the arrow if you are stuck)"
    - Leave the directory `remote-branches` with the command `cd ..`.
    - Run the command `pwd` and you should see `/root/workspace` as a result.
    - If you got a different result, run `cd /root/workspace` to change into the right directory.
    - When you are in the right directory, run `mkdir merge`.
    - Change into the newly created directory with `cd merge`.
    - Run `git init` which creates a new git repository in the current directory.


## Task 2

- Create the following graph. Each commit/node should have the following properties:
    - Node name = commit message
    - Each commit (except merge commits) should create a file with the name of the commit message.
    - Example: commit `C1` creates a new file `C1`.

![Graph](tikz/project_graph.svg)

??? help "Solution (Click on the arrow if you are stuck)"
    - Start with commit `C1`. Create a new file `C1` and commit that file with the message `C1`.
    - In the graph, you can see that `C1` has two successor commits. It is recommended to create a temporary branch, so you can come back to this commit later. Run `git branch branch1`. You created a new branch named `branch1`. You are still on the branch `main`.
    - Create the second commit `C2` with a file called `C2`.
    - For commit `C5` you need `C2` and `C3` as predecessor.
    - Lets create commit `C3`. For that, the previously created branch `branch1` is useful. Change to that branch with `git switch branch1`. You are now again on commit `C1`. Create the commit `C3`.
    - Change back on the branch `main`. Create `C5` with the command `git merge branch1`. This merges the changes of your current `main` branch with the changes of `branch1`. While merging don't forget to adjust the message of the merge commit.
    - Rinse and repeat until the graph is complete.
