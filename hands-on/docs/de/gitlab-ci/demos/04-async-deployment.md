# Sync Deployment

## 4.1 Artifacts

Beispiel
```
stages:
  - build
  - test
  - deploy

build:
  stage: build
  script:
    - echo "building..." >> ./result.txt
  artifacts:
    paths:
    - result.txt
    expire_in: 1 week

unit_test:
  stage: test
  script:
    - ls
    - cat result.txt
    - echo "unit testing..." >> ./result.txt
  artifacts:
    paths:
    - result.txt
    expire_in: 1 week

deploy:
  stage: deploy
  script:
    - ls
    - cat result.txt
```

## 4.2 Docker Build / Registry

- Docker Image bauen

```yml
build:
  image: docker:20.10.16
  script:
    - docker build .
```

- Jetzt mit Test erweitern

```yml
stages:
  - build
  - test

  # build-Job erweitern
  stage: build

test:
  image: demoapp
  stage: test
  script:
    - pip install pytest
    - pytest demoapp
```

- Wie komm ich an das Image?
- Bei der Registry anmelden

```yml
  # Build Job erweitern
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t demoapp .
    - docker push demoapp
```
- Access denied, weil Request geht an dockerhub
- Container-Registry zeigen. Da stehen auch die Namen, die man verwenden soll
- Alternative: `$CI_REGISTRY_IMAGE`, demoapp überall ersetzen
- Commiten und Ergebnis in der CI und Registry zeigen

## 4.3 Services

- Test in test_healthcheck.py einkommentieren
- Testet, ob eine DB verfügbar ist
- Test-Job erweteitern:
```yml
test:
  image: $CI_REGISTRY_IMAGE
  stage: test
  variables:
    MARIADB_ALLOW_EMPTY_ROOT_PASSWORD: "true"
  services:
    - mariadb:10.9.3
  script:
    - export DATABASE_HOST=mariadb
    - pip install pytest
    - pytest demoapp
```

## 4.4 Container-Tag

- Zu guter letzt noch darauf achten, dass sich Branches nicht gegenseitig in die Quere kommen
- `$CI_REGISTRY_IMAGE` -> `$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG`