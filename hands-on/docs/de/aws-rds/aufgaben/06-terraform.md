# Ressourcen mit Terraform anlegen

!!! important "Ziele"
    - Sie erstellen einen "Access Token" für den Zugriff über die AWS-API
    - Sie lernen grundlegende Befehle für Terraform kennen.
    - Sie erstellen eine Subnet Group über Terraform.
    - Zerstören der erstellten Infrastruktur :bomb:

    Optional:

    - Eine RDS DB Instanz mit Terraform erstellen.

!!! help "Hilfsmittel"
    - Sollten Sie Probleme haben die Aufgaben zu lösen, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.
    - Versuchen Sie die Aufgaben erst einmal ohne die Lösungshilfen zu bearbeiten.

## Aufage 1 - AWS Access Token erstellen

Erstellen Sie einen neuen Access Token für Ihren Account über das IAM Access Management.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Navigieren Sie über die Suchleiste zu den **IAM** Einstellungen.
    1. Klicken Sie im linken Menü auf **Users** im Bereich Access Management.
    1. Wählen Sie Ihren User aus: **Student**
    1. Wechseln Sie auf den Reiter: **Security Credentials**
    1. Scrollen Sie zum Punkt **Access keys** und wählen Sie **Create access key** aus.

    ???+ warning "Wichtig!"
        **Schliessen Sie das neue Fenster nicht bevor Sie den Access key kopiert haben.**

        Dieser wird Ihnen nur einmal angezeigt. Sollten Sie das Fenster trotzdem geschlossen oder verloren haben, müssen Sie einen neuen Access key erstellen.

    1. Kopieren Sie sich sowohl die **Access key ID** als auch den **Secret access key** in die Datei `secret.auto.tfvars` in ihrer VS Code Instanz im Pfad `terraform`.

## Aufgabe 2 - Terraform initialisieren

Prüfen Sie die Angaben in den Dateien `secret.auto.tfvars`, `user.auto.tfvars`. Navigieren Sie in einem Terminal in den Terraform Pfad und führen Sie ein `terraform init` aus. Prüfen Sie kurz ob es Probleme gab beim Initialisieren. Planen Sie anschließend das Erstellen der Grundstruktur mit `terraform plan` und überprüfen Sie auch hier die Ausgabe auf Probleme. Erstellen Sie anschließend die Infrastruktur mit dem Befehl `terraform apply`.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Prüfen Sie die Eingaben in der Datei `secret.auto.tfvars` im `terraform` Pfad. Hier solle die **Access key ID** und der **Secret access key** eingetragen sein.
    1. Prüfen Sie die Eingaben in der Datei `user.auto.tfvars` im `terraform` Pfad. Hier sollten wiedererkennbare Werte eingetragen sein. z.B.:
        - **environment**: `terraform-handson`
        - **organisation**: `meine-organization`
    1. Öffnen Sie ggf. über das Menü Links oben ein neues Terminal falls Sie kein aktives Terminal haben sollten.
    1. Navigieren Sie in den terraform-Pfad: `cd ~/workspace/terraform`
    1. Führen Sie `terraform init` aus und prüfen Sie anschließend, ob es ohne Fehler durchgeführt wurde.
    1. Führen Sie den Befehl `terraform apply` aus und prüfen Sie kurz die Ausgabe auf Fehler. Bestätigen Sie anschließend mit `yes` das Terraform die Infrastruktur auf AWS erstellen soll.
    1. Warten Sie bis die Infrastruktur angelegt wurde.

## Aufgabe 3 - DB Subnet Group anlegen
Legen Sie in der Datei `network.tf` eine neue Subnet Group für eine RDS MySQL Datenbank an. Es sollen die Ressourcen: `private-subnet-a`, `private-subnet-b`, `private-subnet-c` in dieser Subnet Group verwendet werden.

Weitere Informationen finden Sie in der offiziellen Dokumentation:
[https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_subnet_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_subnet_group)

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Öffnen Sie die Datei `network.tf` im VS Code Editorfenster.
    1. Fügen Sie dort diesen Codeblock ein:
    ```
    resource "aws_db_subnet_group" "rds-subnet-group" {
      name       = "my-rds-subnet-group"
      subnet_ids = [
        aws_subnet.private-subnet-a.id,
        aws_subnet.private-subnet-b.id,
        aws_subnet.private-subnet-c.id
      ]

      tags = {
        Name = "my-rds-subnet-group"
      }
    }
    ```
    1. Passen Sie ggf. die Werte: `name` sowie den Tag `Name` an.
    1. Planen und wenden Sie die Änderungen mit den Befehlen `terraform plan` sowie `terraform apply` an.
    1. Sie können nun auch in der AWS Konsole prüfen ob die Subnet Group angelegt wurde unter: **RDS/Subnet Groups**.

## Aufgabe 4 (optional) - RDS MySQL Instanz anlegen

Wenn Sie noch Zeit haben, können Sie versuchen eine Datenbank Instanz über Terraform zu erstellen. Legen Sie die Ressource in der Datei `rds.tf` an.

Mehr Informationen zu RDS Instanzen mit Terraform finden Sie in der offiziellen Dokumentation: [https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance)

## Aufgabe 5 - Zerstören

Wenden Sie `terraform destroy` an und zerstören Sie die Infrastruktur. :bomb:
