# Cheatsheet 6.1 - Debugging mit Docker-Compose

| Befehl                                              | Aktion                                                            |
|-----------------------------------------------------|-------------------------------------------------------------------|
| ```docker-compose up <Service...>```                | Einen oder mehrere Services im Vordergrund erstellen und starten  |
| ```docker-compose up -d <Service...>```             | Einen oder mehrere Services im Hintergrund erstellen und starten  |
| ```docker-compose down```                           | Alle Services stoppen und löschen                                 |
| ```docker-compose build <Service...>```             | Einen oder mehrere Services bauen                                 |
| ```docker-compose logs <Service...>```              | Die Logs von einem oder mehreren Services ausgeben                |
| ```docker-compose ps```                             | Die laufenden Container anzeigen lassen                           |
| ```docker-compose top <Service...>```               | Die laufenden Prozesse von einem oder mehreren Services auflisten |
| ```docker-compose rm <Service...>```                | Einen oder mehrere Services löschen                               |
| ```docker-compose exec <Service> <Befehl>```        | Befehl in Container mit TTY ausführen                             |
| ```docker-compose exec -T <Service> <Befehl>```     | Befehl in Container ohne TTY ausführen                            |

# Cheatsheet 6.2 - Debugging mit Docker (nativ)

| Befehl                                              | Aktion                                                       |
|-----------------------------------------------------|--------------------------------------------------------------|
| ```docker run <image>```                            | Container im Vordergrund starten                             |
| ```docker run -d <image>```                         | Container im Hintergrund (`detached`) starten                |
| ```docker stop```                                   | Einen laufenden Container stoppen                            |
| ```docker rm```                                     | Einen gestoppten Container löschen                           |
| ```docker build .```                                | Ein Image aus dem Dockerfile im aktuellen Ordner bauen       |
| ```docker logs <container>```                       | Logs eines Containers anzeigen                               |
| ```docker ps```                                     | Laufende Container anzeigen                                  |
| ```docker top <container>```                        | Laufenden Prozesse von einem Container auflisten             |
| ```docker exec -ti <container> sh```                | Interaktive Shell in Container starten                       |
| ```docker exec -ti <container> sh -c "<befehl>"```  | Befehl von einer Shell im Container ausführen lassen         |
| ```docker stats <container>```                      | Ressourcenauslastung eines Containers anzeigen               |
| ```docker events <container>```                     | Statusevents eines Containers anzeigen                       |
| ```docker inspect <container>```                    | Low-level Informationen eines Containers anzeigen            |
| ```docker history <image>```                        | Einzelnen Layer eines Images anzeigen                        |
