# Merge mit Konflikt

!!! important "Ziel"
    - In diesem Projekt geht es um den Umgang mit Konflikten beim Mergen.

!!! help "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
     [Folien](https://slides.corewire.de/presentations/git/4-branches-2) und des
     [Cheatsheets](../cheatsheets/3-branches.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1

- Verlassen Sie das Repository `merge` und legen Sie ein neues an:

    ```bash
      cd ..           # Den Order merge verlassen
      mkdir conflict  # Einen neuen Ordner anlegen
      cd conflict     # In den neuen Ordner wechseln
      git init        # Ein Git-Repository anlegen
    ```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Verlassen Sie den Ordner `merge` mit dem Kommando `cd ..`.
    - Führen Sie `pwd` aus. Sie sollten die Augabe `/root/workspace` erhalten.
    - Sollten Sie die Ausgabe nicht erhalten, wechseln Sie mit `cd /root/workspace` in das Verzeichnis.
    - Wenn Sie im richtigen Verzeichnis sind, führen Sie `mkdir conflict` aus.
    - Wechseln Sie mit `cd conflict` in den neu angelegten Ordner.
    - Führen Sie `git init` aus. Dadurch wird das Verzeichnis zu einem Git-Repository und Sie können Dateien/Änderungen committen.

## Aufgabe 2

- Erstellen Sie eine Datei `teilnehmer.txt` mit folgendem Inhalt:

    ```txt
    Training 1
      Philipp
      Lisa
      Rebecca
      Thomas
      Alexander
    ```

## Aufgabe 3

- Erstellen Sie einen Commit mit den Änderungen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Erstellen Sie einen Commit mit `git add .` und `git commit -m "{Hier Ihre Commit-Nachricht einfügen}"`.

## Aufgabe 4

- Erstellen Sie einen neuen Branch `training2` und wechseln Sie auf diesen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git switch -c training2` aus.

## Aufgabe 5

- Fügen Sie zu der Datei `teilnehmer.txt` folgenden Inhalt hinzu:

    ```txt
    Training 2
      Markus
      Ramona
      Till
      Juliane
    ```

## Aufgabe 6

- Erstellen Sie einen Commit mit den Änderungen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Erstellen Sie einen Commit mit `git add .` und `git commit -m "{Hier Ihre Commit-Nachricht einfügen}"`.

## Aufgabe 7

- Wechseln Sie auf den `main`-Branch.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git switch main` aus.

## Aufgabe 8

- Löschen Sie die Teilnehmer `Thomas` und `Alexander`. Erstellen Sie einen Commit
   mit den Änderungen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Löschen Sie die Namen aus der Datei und speichern Sie diese.
    - Erstellen Sie einen Commit mit `git add .` und `git commit -m "{Hier Ihre Commit-Nachricht einfügen}"`.

## Aufgabe 9

- Mergen Sie die Änderungen von dem Branch `training2` in den `main`-Branch.
   Lösen Sie den Konflikt.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Starten Sie den Merge mit `git merge training2`.
    - Öffnen Sie die Datei im Editor. VS Code markiert die Stelle mit dem Konflikt automatisch.
    - Wie Sie sehen enthält die Änderungen vom Branch `training2` nicht nur die Änderungen, die auf dem Branch gemacht wurden, sondern auch die in `main` bereits gelöschten Namen.
    Diese Änderung möchten wir nicht in `main` mergen.
    - Entfernen Sie deshalb die Namen `Thomas` und `Alexander`, sodass nur die eigentliche Liste "Training 2" übrig bleibt und speichern Sie.
    - Markieren Sie den Konflikt mit `git add teilnehmer.txt` als gelöst.
    - Beenden Sie mit `git commit -m "{Hier Ihre Commit-Nachricht einfügen}"` den Merge.
