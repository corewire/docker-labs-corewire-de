# Docker & Container

- [Zu den Aufgaben](./aufgaben/01-erste-schritte-mit-docker.md)
- [Zu den Cheatsheets](./cheatsheets/01-erste-schritte.md)
- [Zu den Folien](https://slides.corewire.de/presentations/docker/01-einfuehrung/#/)

## Dauer
2 Tage

## Zielgruppe
- Softwareentwickler:innen
- Software-Architekt:innen
- Systemadministrator:innen
- DevOps-Engineers

## Voraussetzungen
- (optional) Erste Erfahrungen mit dem Terminal/Bash
- (optional) Erste Erfahrungen mit Docker

## Kursziel
Der Kurs vermittelt den Umgang und technische Hintergründe von Docker in der Praxis. Am ersten Tag werden Anwendungsfälle von Containern und Grundlagen behandelt. Die Anwendungsfälle geben einen Einblick, in welchen Situationen die Verwendung von Containern sinnvoll ist. Die Grundlagen ermöglichen den Teilnehmer:innen den sicheren Umgang mit Containern. Der zweite Tag beinhaltet das Erstellen von eigenen Images, sowie einen tiefen Einblick in das Debugging mit Containern, sodass Fehler, die bei der Erstellung und dem Betrieb von Containern auftreten können, gefunden und behoben werden können. Der Kurs richtet sich somit auch explizit an Teams mit unterschiedlichen Erfahrungsleveln zu Docker und Container.

## Schulungsform
Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **60% theoretischen Inhalten** und **40% praktischen Übungen**.

## Kursinhalt
- Grundkonzepte und Anwendungsfälle für Container
- Grundlagen zu Images, Volumes und Netzwerken
- Service-Architekturen mit docker-compose abbilden
- Designen von containerbasierten Anwendungen
- Erstellen von eigenen Images (Dockerfile) mit Layern, Caching und Multistage
- Erweitertes Debugging von Containern
- Security Best Practices
- Container unter Windows
- Ausblick: Container in CI/CD
