# Basic Commands

!!! important "Goal"
    - This project is about the basic functionality of git. You will change/modify files.
    - After every action run `git status` to see the results of your changes. In
    the end you'll create a commit with all your changes and send it to the
    server by pushing it.

!!! help "Hints"

    - Try to solve the following tasks with the help of the [Slides](https://slides.corewire.de/presentations/git/1-introduction)
    and the [Cheatsheets](../cheatsheets/1-introduction.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

Work on the following tasks and run `git status` after every action that you take.

## Task 1

- Change the content of `files/change_me.txt` and save it.
- Let git show you the changes that you made.

??? help "Solution (Click on the arrow if you are stuck)"
    - Open the file `change_me.txt` in the file browser on the left side at `my-fist-project/files`.
    - Change the content and save it with `Ctrl + s`.
    - Click on the terminal and make sure that you are in the folder `~/workspace/my-first-project`.
    - Run `git status`. The output should include `modified: files/change_me.txt`.

## Task 2

- Move the file `files/move_me.txt` to a different location. Use the respective git command for that.
- Let git show you the changes that you made.

??? help "Solution (Click on the arrow if you are stuck)"
    - Run `git mv files/move_me.txt files/new_location.txt`.
    - Run `git status`.
    The file was renamed and added to the staging-area automatically.

## Task 3

- Delete the file `files/delete_me.txt`.
- Let git show you the changes that you made.

??? help "Solution (Click on the arrow if you are stuck)"
    - In the file browser look for `delete_me.txt`.
    - Delete the file by right clicking on it and pressing `Delete Permanently`.
    - Run `git status`. The file is shown as `deleted`.

## Task 4

- Rename the file `files/rename_me.txt` with the file browser on the left side
of VS Code.
- Let git show you the changes that you made. The old file should be marked as `deleted` and the new one as `untracked`.
- Add both files to the staging area.
- Let git show you the changes again. Both files should now be shown in the `Changes to be committed` area with the status `renamed`.

??? help "Solution (Click on the arrow if you are stuck)"
    - In the file browser search for `rename_me.txt`.
    - Rename the file by right clicking on it and pressing `Rename`.
    - Switch to the terminal and run `git status`. The old file should be shown
    as `deleted` and the new one as `untracked`.
    - Add both files to the staging area by running `git add files/rename_me.txt` and `git add files/{new_name}`
    - Check the resulting state again by running `git status`. Both files should
    now be listed under `Changes to be committed` with the status `renamed`.

## Task 5

- Create a new file `files/new.txt` and add it to the repository.
- Let git show you the changes that you made.

??? help "Solution (Click on the arrow if you are stuck)"
    - Create a new file by right clicking and pressing `New File` on the directory `files`.
    - Name the file `new.txt`.
    - Run `git status`. The file will now be listed under `Untracked files`.

## Task 6

- Create a commit with all changes that you made so far.

??? help "Solution (Click on the arrow if you are stuck)"
    - Add all changes that you made in the directory `files` to the staging area
    by running `git add files`.
    - With `git diff --staged` you can show all the changes that will be part of the commit. With `q` you can exit.
    - Create a commit by running `git commit -m "My first commit"`.

## Task 7

- View your new commit in the commit history.
- The name and the email address are currently automatically configured.

??? help "Solution (Click on the arrow if you are stuck)"
    - Run `git log`. Latest commits are on top. With `q` you can exit.
    - The commit that you just created will be shown.

## Task 8

- Change the git identity to use your name and arbitrary email address.

??? help "Solution (Click on the arrow if you are stuck)"
    - Change your name with `git config --global user.name <Name>`.
    - Change your email address with `git config --global user.email arbitrary@example.com`.

## Task 9

- Create the file `files/ignored.txt`. This file should **not** be added to any
commit and also not shown by `git status`.

??? help "Solution (Click on the arrow if you are stuck)"
    - Create a file `ignored.txt` in the folder `files`.
    - Run `git status`. The file will be shown under `Untracked files`.
    - Create the file `.gitignore` in the folder `my-first-project`.
    - Add the following line to the `.gitignore` file:

        ```text
        files/ignored.txt
        ```

    - Run `git status` again. The file `ignored.txt` should no longer be listed.

## Task 10

- Create a second commit. It should contain only the file `.gitignore`.
- When your run `git status` afterwards, no changes should be visible.

??? help "Solution (Click on the arrow if you are stuck)"
    - Add `.gitignore` to the staging area by running `git add files`.
    - Run `git diff --staged` to show what changes will be committed.
    - Run `git commit`. The `nano` terminal editor opens up.
    - Enter a commit message like `"Add .gitignore"`.
    - Save with `Ctrl + o` and `Enter`.
    - Leave the terminal editor with `Ctrl + x`.

## Task 11

- Check your commit history again. A new commit with your name as an author
should be visible.

??? help "Solution (Click on the arrow if you are stuck)"
    - Run `git log`.
    - The commit created by you will be shown. This should show your name as author.

## Task 12

- Push your changes to the server. In Gitlab you can verify that your changes
were successfully sent to the server.

??? help "Solution (Click on the arrow if you are stuck)"
    - Run `git push`.
    - Switch to the Gitlab web interface.
    - On the top left corner under `Projects` look for `my-first-project`.
    - If `my-first-project` is not shown directly, click on `Your projects` and you will find it there.
    - View the commit.
