# Dockerfile

Dockerfile liegt im [Repo](https://gitlab.com/corewire/hands-on/docker-demoapp/-/blob/main/Dockerfile). Kann einfach Schritt für Schritt aufgebaut werden:

Vorschlag 1. Version:

```docker
FROM python:3.10-slim

WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY ./dockerdemo ./dockerdemo

CMD ["flask", "run"]
```

Starten, geht kaputt. Folgendes hinzufügen

```docker
ENV FLASK_APP=dockerdemo/app
```

Ist aber noch nicht erreichbar. CMD erweitern:

```docker
CMD ["flask", "run", "--host=0.0.0.0"]
```