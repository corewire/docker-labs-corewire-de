# Schulungsumgebung

## VSCode zeigen
- Wie komme ich drauf?
- Wo ist was?

## Container in VSCode

- Nginx-Container starten

```
docker run -p 8080:80 nginx
```

- Wie erreiche ich den Container über das Internet?

<http://code-0.{{ domain }}:8080/>

**Wichtig:** http **nicht** https

## Docker logs

- Nginx-Container von vorher läuft im Vordergrund -> Zugriffslogs zeigen
- Container detached starten, `docker logs` zeigen, einmal ohne und einmal mit `-f`

```
docker run -d -p 8080:80 nginx
```

```
docker logs
```

```
docker logs -f
```

## Demo-Anwendung

- Demo-Anwendung starten und zeigen

```
docker run -p 8080:5000 corewire/docker-demoapp
```

<http://code-0.{{ domain }}:8080/>

- Container stoppen und löschen. Neuen Container starten. Notizen sind weg 
