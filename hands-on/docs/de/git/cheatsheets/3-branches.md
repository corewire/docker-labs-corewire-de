# Cheatsheet 3 - Branches

## Teil 1

| Befehl                                          | Aktion                                                                                                  |
|-------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| ```git switch <branch>```                     | Auf lokal bestehenden Branch wechseln oder remote existierenden Branch lokal anlegen und drauf wechseln |
| ```git switch -c <branch>```                  | Neuen Branch anlegen und drauf wechseln                                                                 |
| ```git branch```                                | Verfügbare Branches anzeigen                                                                            |
| ```git branch -a```                             | Alle Branches anzeigen (auch Remote)                                                                    |
| ```git branch <branch>```                       | Neuen Branch anlegen                                                                                    |
| ```git branch -d <branch>```                    | Branch löschen                                                                                          |
| ```git fetch [<remote>] [<branch>]```           | Änderungen vom Server holen                                                                             |
| ```git push [<remote>] [<branch>]```            | Änderungen auf den Server schieben                                                                      |
| ```git push --set-upstream <remote> <branch>``` | Lokalen Branch auf den Server schieben                                                                  |
| ```git merge <branch>```                        | `<branch>` in aktuellen Branch mergen                                                                   |
| ```git merge --no--ff <branch>```               | Merge der keinen fast-forward-merge macht                                                               |

## Teil 2

| Befehl                            | Aktion                                               |
|-----------------------------------|------------------------------------------------------|
| ```git rebase <branch>```         | Den aktuellen Branch an `<branch>` anhängen          |
| ```git pull --rebase <branch>```  | Änderungen vom Server holen und rebase ausführen     |
| ```git stash```                   | Nicht-committete Änderungen temporär "beiseitelegen" |
| ```git stash pop```               | Letzten Eintrag vom Stash holen und anwenden         |
