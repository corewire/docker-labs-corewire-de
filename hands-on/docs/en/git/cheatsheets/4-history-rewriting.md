# Cheatsheet 4 - History Rewriting

| Command                                                          | Action                                                                                 |
|------------------------------------------------------------------|----------------------------------------------------------------------------------------|
| ```git rebase -i HEAD~4```                                       | Interactive rebase of the last 4 commits                                               |
| ```git cherry-pick <commit-hash> [...more commit hashes...]```   | Apply a single commit to HEAD                                                          |
|                                                                  |                                                                                        |
| ```git apply <patchfile>```                                      | Apply patch-file                                                                       |
| ```git format-patch <branch> -o <dir>```                         | Generate a patch-file in `<dir>` from the differences of current branch and `<branch>` |
| ```git format-patch <branch> -1 <commit_hash> -o <dir>```        | Generate a patch-file for `<commit_hash>` in `<dir>`                                   |
| ```git am <patchfiles>```                                        | Apply patch-files and create commit from them                                          |
|                                                                  |                                                                                        |
| ```git filter-branch --tree-filter <commmand> HEAD```            | Check out all parent-commits and apply `<command>`                                      |
| ```git filter-branch --all --tree-filter <command>```            | Check out all commits and apply `<command>`                                             |
| ```git filter-branch --index-filter <command> HEAD```            | Apply `<command>` on all parent-commits in INDEX                                       |
|                                                                  |                                                                                        |
| ```git push --force-with-lease```                                | Overwrite remote versions DB from local                                                |
|                                                                  |                                                                                        |
| ```git reflog```                                                 | Show the reflog for HEAD                                                               |
| ```git reflog show <branch>```                                   | Show the reflog for `<branch>`                                                         |
| ```git reflog stash```                                           | Show the reflog for the stash                                                          |
