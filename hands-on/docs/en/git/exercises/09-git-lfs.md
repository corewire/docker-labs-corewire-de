# Git LFS & Bisect

!!! important "Goal"
    - In this project you will learn:
        - How to use git-LFS. For that we will add a website to our versioning.
        - How to use git bisect.

## Task  1 - Git LFS

First we need to create a new repository.

- Create a new folder called `website` in VS Code.
- Navigate into the new folder and make a repository out of it with `git init`.

- Now we need a website. On <https://freehtml5.co/> you can find many templates
for websites. Just search for a template you like.

- Click on the download button and copy the download URL with a right click and a click on copy link url.
- Download the template as a `zip`-file with:

```bash
curl --output website.zip <URL>
```

- Extract the zip file with:

```bash
unzip website.zip
```

- Remove the `website.zip`.

## Task  2

Now we have all files we need in our repository and as a next step we want to
commit these files. Nevertheless we also want all binary files to be deposited
with git LFS. The first step is to tell git that we are
going to use LFS in this repository:

```bash
git lfs install
```

Now we need to tell git what binary files we want to manage with LFS.
This is possible to configure with any file extension as follow:

```bash
git lfs track "-.<file-extension>"
```

Important: don't forget the quotation marks. Which file
extensions we need to track is depending on the template that was
downloaded earlier. The results of the track commands can be
reviewed in the file `.gitattributes`. Now we can commit our changes:

- Commit the website.

## Task 3

We will now push our repository to the Gitlab server. On the server you will find a project with the name `git-lfs`.  
On the landing page of the project you are instructed on how to push an
already existing repository to this project.

- Push the repository.

Note the output on the command line regarding the processing of the LFS files.
On the Gitlab project page you can see those LFS files as well.
These are also marked as LFS managed there. If someone else, with
installed `git lfs`, will check out this repository in the future,
everything will setup and work out of the box.

## Task 4 - Bisect preperations

Search in Gitlab for the project `bisect` and clone it.

??? help "Solution (Click on the arrow if you are stuck)"
- Search in Gitlab in the left upper corner `Menu->Projects` for the project `bisect`.
  If you don't find the project directly here, you can find it under `Your projects`.
- Copy the link from `Clone with HTTPS`.
- Switch back to the terminal of the VS Code instance and ensure that the current working directory is `/root/workspace`.
  Therefore move upwards from previous repositories with `cd ..` or alternatively switch directly into the directory with: `cd /root/workspace`.
- Clone the project with the command `git clone {URL}`.

## Task 5 - Manual bisect

A fellow developer is asking you for help. He noticed that something
is broken on his branch but cannot find the commit that started this change.
All you know is the following:

- On `main` it is working.
- On his branch 'bisect_manual' it doesn't work.
- The branch 'bisect_manual' is 50 commits ahead of `main`.

For simplicity the branch only contains a file named test.txt. 
It contains either 'Here is my good code...' or 'Here is my bad code...'. 
Your task is to find the commit that introduced the change from 'good' to 'bad'.

- Start by switching to the branch 'bisect_manual'.
- Take a look at the file test.txt (`cat test.txt`).
- Begin the search with `git bisect start`.
- Mark the known last good commit `git bisect good <id>` and the current bad commit with `git bisect bad <id>`.

??? help "Solution (Click on the arrow if you are stuck)"
    - To start the bisect process, run the following commands:
        ```bash
        git bisect start
        git bisect good main
        git bisect bad HEAD
        ```
    - Git checks out a commit in between `main` and HEAD. Check if that commit is
    good or bad with `cat test.txt` and mark the commit accordingly with `git bisect good`
    or `git bisect bad`.

    - Repeat the process until you receive the commit which broke your code.

Once finished, inspect the changes of the commit you found and stop the bisect process.

??? help "Solution (Click on the arrow if you are stuck)"
    - Verify the changes in the commit with `git show <id>`.
    - End the bisect process with `git bisect reset`.


## Task 6 - Automated bisect

Did you find it tedious to check every commit by hand?

Bisect comes with a 'run' option that helps you automate the process. This can come
in handy if you want to find a commit in a bigger part of your project history.
This time the branch has 50.000 commits. In addition to the test.txt, there is also a
check_commit.sh script. Its purpose is to distinguish between good and bad commits.

- Start by switching to the branch `bisect_auto`.
- Take a look at the branch.
- Take a look at the script.
- Begin the search with `git bisect start`
- After you tagged a good and a bad commit, try `git bisect run ...`.

??? help "Solution (Click on the arrow if you are stuck)"
    - To start the bisect process, run the following commands:
        ```bash
        git bisect start
        git bisect good main
        git bisect bad HEAD
        ```
    - To start the automation with the script run:
        ```bash
        git bisect run ./check_commit.sh
        ```
