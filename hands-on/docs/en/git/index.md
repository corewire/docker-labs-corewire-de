# Git - Decentralized versioning

- [Exercises](./aufgaben/01-mein-erstes-projekt.md)
- [Cheatsheets](./cheatsheets/1-einfuehrung.md)
- [Slides](https://slides.corewire.de/presentations/git/1-introduction/#/)

## Targetgroup
- Software developer
- System administrator
- DevOps-Engineers

## Duration
2 days

## Prerequisite
- (optional) First experience with Terminal/Bash
- (optional) First experience with Git

## Course target
The course teaches how to use Git in practice. The first day covers the basics and contexts for the daily use of Git. The second day includes teamwork and advanced functionality with which you develop an understanding of the technical background of Git in order to be able to react to problems that have not been dealt with. The course is thus also explicitly aimed at teams with different levels of experience with Git.

## Form of training
The trainer presents the training content with slides and live demos. Between the individual chapters, the participants have the opportunity to apply the contents themselves in practical exercises. The distribution is **60% theoretical content** and **40% practical exercises**.

## Course content

- Basic concepts of decentralised versioning
- Git commit/push/pull and other basics of Git
- Branching, as well as classification in project organisation and merging of more complex conflicts
- Rebase, comparison to merging and best practices including special cases and pitfalls
- History rewriting and force push, dangers and potentials, as well as use in teamwork
- Use of GitLab and comparable interfaces (GitHub) for team organisation and project management
- Branching workflows such as Gitflow and Pull Requests, with practical parts in GitLab
- Advanced Git features like cherry picking, interactive rebase, bisect, stash and more
