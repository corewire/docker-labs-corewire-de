# Mein erstes Projekt

!!! important "Ziel"
    - In diesem Projekt sollen Sie sich mit der Schulungsumgebung, git in der Kommandozeile und Gitlab vertraut machen.
    - Nehmen Sie sich Zeit und schauen Sie alles in Ruhe an.

## Aufgabe 1

- Die Schulungsmaschinen sind unter `code-{ZAHL}.{{ domain }}` zu erreichen.
Ersetzen Sie `{ZAHL}` mit der ihnen zugewiesenen Zahl.
  - Beispiel für die Zahl 5: `code-5.{{ domain }}`.
  - Beispiel für die Zahl 11: `code-11.{{ domain }}`.
- Geben Sie danach das ihnen zugewiesene Passwort ein.
- Sie haben nun Zugriff auf die Schulungsmaschine.

## Aufgabe 2

- Loggen Sie sich nun auch im Gitlab ein. Das geht über den Button rechts oben.
Verwenden Sie dafür `user-{ZAHL}` als Username und das Passwort von eben.
- Sie sehen nun mehrere Projekte. Fürs erste wollen wir das Projekt `my-first-project` klonen.
- Öffnen Sie dafür das Projekt `my-first-project` mit einem Klick. Sie sehen nun das Projekt in einer Übersicht mit dem Name,
 ein paar Statistiken (Anzahl Commits, Anzahl Branches, Tags und die Größe) und weiter unten den enthaltenen Dateien.
- Zwischen dem Projektnamen und dem Dateibrowser befindet sich auf der rechten Seite ein blauer Button `Clone`.
- Klicken Sie ihn und kopieren Sie anschließend den Link unter `Clone with HTTPS`.
- Gehen Sie nun wieder zurück zu VS Code (der Schulungsmaschine).
- Sofern noch kein Terminal vorhanden ist, öffnen Sie eins über den Reiter links oben.
- Klonen Sie anschließend das Projekt durch Eingabe von `git clone {URL}` im Terminal.
 Einfügen im Terminal ist mit der Tastenkombination `Strg (+ Umschalt) + v` möglich.
- Wenn alles geklappt hat, sollten Sie nun im Dateibrowser einen Ordner `my-first-project` sehen.

## Aufgabe 3 - Erste Kommandos in Git

- Im Terminal, wechseln Sie in das Projektverzeichnis.

   ```bash
   cd my-first-project
   ```

- Führen Sie anschließend `git status` aus.

  ```bash
  $ git status
  On branch main
  
  nothing to commit, working tree clean
  ```

- Wenn Ihre Ausgabe genauso aussieht, dann hat alles funktioniert.
    Sie haben sich erfolgreich eingeloggt, das Projekt geklont und können nun mit dem Projekt arbeiten.
