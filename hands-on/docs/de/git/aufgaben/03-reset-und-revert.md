# Reset und Revert

!!! important "Ziel"
    - In diesem Projekt geht es um den Umgang mit Änderungen und wie man diese auf unterschiedlichen Wegen
     rückgängig machen kann.

!!! help "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
     [Folien](https://slides.corewire.de/presentations/git/2-aenderungen) und des
     [Cheatsheets](../cheatsheets/2-aenderungen.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1

- Verlassen Sie das Repository `my-first-project` und legen Sie ein neues an:

   ```bash
   cd ..                  # Den Order my-first-project verlassen
   mkdir reset_und_revert # Einen neuen Ordner anlegen
   cd reset_und_revert    # In den neuen Ordner wechseln
   git init               # Ein Git-Repository anlegen
   ```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Verlassen Sie den Ordner `my-first-project` mit dem Kommando `cd ..` (`cd` steht für `Change Directory` und `..` für das Elternverzeichnis).
    - Führen Sie `pwd` aus. `pwd` liefert ihnen das aktuelle Verzeichnis.
    Sie  sollten die Ausgabe `/root/workspace` erhalten.
    - Sollten Sie die Ausgabe nicht erhalten, wechseln Sie mit `cd /root/workspace` in das Verzeichnis.
    - Wenn Sie im richtigen Verzeichnis sind, führen Sie `mkdir reset_und_revert` aus (`mkdir` steht für `make directory` und erstellt einen Ordner).
    - Wechseln Sie mit `cd reset_und_revert` in den neu angelegten Ordner.
    - Führen Sie `git init` aus. Dadurch wird das Verzeichnis zu einem
      Git-Repository und Sie können Dateien/Änderungen committen.

## Aufgabe 2

- Committen Sie eine neue, leere Datei.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Erstellen Sie über den Dateibrowser im Verzeichnis `reset_und_revert` eine neue leere Datei.
    - Fügen Sie die Datei mit `git add .` zur Staging-Area hinzu. Vergessen Sie hierfür nicht den Punkt hinter `add`.
    - Überprüfen Sie das mit `git status`.
    - Erstellen Sie einen Commit mit `git commit -m "{Hier Ihre Commit-Nachricht einfügen}"`.
    - Schauen Sie sich ihren Commit mit `git log` an.

## Aufgabe 3

- Ändern Sie die Datei mit einem Commit "Neue Änderung".

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Fügen Sie einen beliebigen Inhalt in die eben erstellte Datei ein und
      speichern Sie.
    - Fügen Sie die Datei zur Stageing-Area hinzu.
    - Erstellen Sie einen Commit mit der Nachricht "Neue Änderung".
    - Schauen Sie sich ihre Commits mit `git log` an.

## Aufgabe 4

- Ändern Sie nachträglich die Commit-Nachricht auf "Neue Datei hinzugefügt".

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git commit --amend` aus.
    - Sie erhalten die gleiche Ausgabe wie bei einem Commit und können nun die
      Commit-Nachricht beliebig bearbeiten.
    - Schauen Sie sich ihre Commits mit `git log` an.

## Aufgabe 5

- Entfernen Sie den Commit wieder komplett, sodass **nichts davon im git log verbleibt**.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Mit `git reset HEAD~1` können Sie den Commit wieder entfernen. Die Änderung
      bleibt dabei in Ihrem Workspace erhalten, sodass Sie sie erneut committen
      können.
    - Schauen Sie sich das Ergebnis mit `git log`, `git status` und `git diff` an.

## Aufgabe 6

- Wiederholen Sie die Schritte aus Aufgabe 3.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Erstellen Sie erneut einen Commit mit einer Änderung in ihrer Datei.

## Aufgabe 7

- Entfernen Sie den Commit nun so, dass am Ende **ein Revert-Commit im git log verbleibt**.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git revert HEAD` aus. Es wird automatisch ein Commit erstellt,
      der die Änderungen wieder Rückgängig macht.
    - Ihre Datei ist nun wieder leer. Die Historie, wie ihre Datei bearbeitet wurde, wurde allerdings erhalten.
