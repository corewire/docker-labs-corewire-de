# Cheatsheet 5 - Advanced Git Features

| Command                               | Action                                                                                 |
|---------------------------------------|----------------------------------------------------------------------------------------|
| ```git checkout HEAD~```              |                                                                                        |
| ```git checkout HEAD^```              | Switch to the predecessor of HEAD                                                      |
| ```git checkout main~2```           |                                                                                        |
| ```git checkout main^^```           | Switch to the pre-predecessor of main                                                |
|                                       |                                                                                        |
| ```git add -p```                      | Partial and interactive staging of files                                               |
|                                       |                                                                                        |
| ```git submodule add <url>```         | Add a submodule to the repository                                                      |
| ```git submodule init```              | Initialize submodules (updates .gitmodules)                                            |
| ```git submodule update```            | Update submodules, so it has the expected status of the parent-project                 |
|                                       |                                                                                        |
| ```git bisect start```                | Start a bisect                                                                         |
| ```git bisect good <commit>```        | Mark the initial good commit                                                           |
| ```git bisect bad <commit>```         | Mark the initial bad commit                                                            |
| ```git bisect good```                 | Mark the active commit as good                                                         |
| ```git bisect bad```                  | Mark the active commit as bad                                                          |
| ```git bisect run <command>```        | Automatic run of bisect (Return code: good (== 0) or bad (!= 0)                        |
|                                       |                                                                                        |
| ```git lfs install .```               | Install lfs for the active repository                                                  |
| ```git lfs track '*.png'```           | Track all png-files with lfs                                                           |
|                                       |                                                                                        |
| ```git log -3```                      | Shows the last 3 commits                                                               |
| ```git log --after="2019-7-1"```      | Shows all commits after "2019-7-1"                                                     |
| ```git log --author="Alice"```        | Shows all commits from author 'Alice' (regular expressions are possible)               |
| ```git log --grep="Closes #1337"```   | Shows commits which contain "Closes #1337" in their commit-message                     |
| ```git log -- document.txt code.py``` | Shows commits for the specified file(s)                                                |
| ```git log main..feature```         | Shows the differences between the branch "main" and "feature"                        |
| ```git log --no-merges```             | Shows non-merge commits                                                                |
| ```git log --merges```                | Shows merge commits                                                                    |
