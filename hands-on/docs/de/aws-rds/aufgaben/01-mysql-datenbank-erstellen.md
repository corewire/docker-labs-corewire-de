# RDS MySQL Instanz erstellen

!!! important "Ziele"
    - Anmelden in der AWS Console
    - Eine neue "Security Group" für eine Datenbank-Instanz erstellen.
    - Eine neue "Subnet Group" für eine Datenbank-Instanz erstellen.
    - Eine Aurora MySQL Datenbank-Instanz konfigurieren und starten.
    - Auf die Datenbank-Instanz mit einem Client zugreifen.
    - Eine Datenbank mit Tabellen auf der Datenbank-Instanz anlegen.

!!! help "Hilfsmittel"
    - Sollten Sie Probleme haben die Aufgaben zu lösen, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.
    - Versuchen Sie die Aufgaben erst einmal ohne die Lösungshilfen zu bearbeiten.

## Aufgabe 1 - Anmelden und Netzwerk vorbereiten

### Anmelden in der AWS Console

Melden Sie sich in der [AWS Console](https://eu-central-1.console.aws.amazon.com/console/home?region=eu-central-1#) an und wechseln Sie zur Region **Frankfurt (eu-central-1)** sowie zur Sprache **English (US)**.
??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Navigieren Sie im Browser zur [AWS Console](https://eu-central-1.console.aws.amazon.com/console/home?region=eu-central-1#).
    1. Wählen Sie **IAM-Benutzer** aus und geben Sie Ihre Konto-Nr./Account-ID oder den Konto-Alias ein und bestätigen Sie mit **weiter**.
    1. Im neuen Fenster tragen Sie Ihren Benutzernamen und das Passwort das Sie erhalten haben ein und bestätigen Sie dies mit **Melden Sie sich an**.
    1. Prüfen Sie nach dem Einloggen die Region im rechten Oberen Bildschirmrand. Wechseln Sie ggf. von **US East N. Virginia** nach **Frankfurt (eu-central-1)**.
    1. Wechseln Sie im linken unteren Bereich die Sprache auf **English (US)**.

### "Security Group" für Datenbank-Instanz anlegen

Erstellen Sie im Bereich "VPC/Security/Security Groups" eine neue Security Group mit dem Namen **{{ training_name }}-labs-rds-sec-group** für das VPC **{{ training_name }}-labs-vpc**, sodass die EC2 Instanz mit der Security Group **{{ training_name }}-labs-http-ssh-sec-group** die Datenbank erreichen kann.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Tippen Sie **VPC** in die Suchleiste am oberen Bildschrimrand und wählen Sie den entsprechenden Menüpunkt aus um auf die VPC Einstellungsseite zu gelangen.
    1. In den VPC Einstellungen navigieren Sie am linken Bildschirmrand in den Bereich **Security** und wählen Sie den Menüeintrag **Security Groups** *oder* klicken Sie in der Übersicht direkt auf den Eintrag **Security Groups**.
    1. Öffnen Sie den Assistent zum Erstellen einer neuen Security Group mit einem Klick auf den orangen Knopf **Create Security Group** am rechten oberen Bildschirmbereich.
    1. Geben Sie als Namen **{{ training_name }}-labs-rds-sec-group** ein.
    1. Geben Sie in der **Description** eine beliebige sinnvolle Beschreibung ein.
    1. VPC: Wählen Sie das VPC **{{ training_name }}-labs-vpc** aus.
    1. Erstellen Sie eine neue **Inbound Rule** mit einem Klick auf den Knopf **Add rule**.
        1. Wählen Sie als Type **MYSQL/Aurora** aus.
        1. Wählen Sie im Suchfeld zur Source die Security Group **{{ training_name }}-labs-http-ssh-sec-group** aus.
    1. Prüfen Sie kurz die **Outbound Rules** ob dort ein Eintrag für **All traffic** von **0.0.0.0/0** hinterlegt ist.
    1. Erstellen Sie die Security Group mit einem Klick auf **Create security group** rechts unten.


### RDS "Subnet Group" anlegen

Erstellen Sie eine neue DB-Subnetzgruppe mit dem Namen **{{ training_name }}-labs-rds-subnet-group** für Ihre MySQL Datenbank im RDS-Bereich.

???+ summary "Subnetz Gruppen Einstellungen"
    === "Subnet group details"
        - Name: **{{ training_name }}-labs-rds-subnet-group**
        - Description: Geben Sie eine kurze Beschreibung ein. (zwingend)
        - VPC: wählen Sie **{{ training_name }}-labs-vpc** aus.
    === "Add Subnets"
        - Availability Zones: Wählen Sie alle 3 Zonen aus - `eu-central-1a`, `eu-central-1b`, `eu-central-1c`
        - Subnets: Wählen Sie die Subnetze mit den CIDR Adressbereichen `10.0.4.0/22`, `10.0.8.0/22`, `10.0.12.0/22` aus.
    Überprüfen Sie Ihre Angaben und bestätigen Sie das erstellen der Subnetz-Gruppe mit **create**.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Suchen Sie in der Haupt-Suche nach **RDS** und wählen Sie den entsprechenden Eintrag aus.
    1. Wählen Sie im linken Bereich den Eintrag **Subnet Groups** aus.
    1. Klicken Sie auf den orangenen Knopf **Create DB subnet group**.
    1. Tragen Sie die Einstellungen wie oben beschrieben ein.


## Aufgabe 2 - MySQL Instanz in RDS erstellen
1. Falls Sie nicht im RDS Bereich sein sollten navigieren Sie über die Suche dort hin.
1. Wählen Sie im linken Menü den Eintrag **Datenbanken** aus.
1. Orangener Button **Datenbank erstellen** anklicken.
1. Im neuen Fenster diese Einstellungen Anwenden:

    ??? summary "Datenbank Instanz Einstellungen"
        === "Engine options"
            - Type: **Amazon Aurora**
            - Edition: **Amazon Aurora MySQL-Compatible Edition**
            - Capacity Type: **Provisioned**
        === "Settings"
            - DB cluster identifier: **{{ training_name }}-labs-mysql-db**
            - Master username: Sie können `admin` verwenden oder auch anpassen, notieren Sie sich den Usernamen.
            - Master password: Vergeben Sie ein Passwort und notieren Sie es sich.
            ???+ warning "Beachten - WICHTIG!"
                Das Passwort kann nachträglich nicht mehr geändert werden! Wenn Sie das Passwort vergessen, muss die Instanz neu erstellt werden.

                Richtlinien für das Passwort: mindestens 8 Zeichen. Nicht erlaubte Zeichen: / (slash), '(single quote), "(double quote) und @ (at Zeichen)
            - Master password confirm: Tragen Sie das Passwort nochmals ein zur Bestätigung.
        === "Availability & durability"
            Multi AZ deployment: **Don't create an Aurora Replica** auswählen.
        === "Connectivity"
            - Virtual private cloud (VPC): **{{ training_name }}-labs-vpc**
            - Subnet Groups: Wählen Sie die Subnetz Gruppe **{{ training_name }}-labs-rds-subnet-group** aus.
            - VPC security groups: Wählen Sie die Security Gruppe **{{ training_name }}-labs-rds-sec-group** aus und entfernen Sie die Gruppe <del>**default**</del>.

1. Überprüfen Sie nochmals ihre Angaben und bestätigen Sie das erstellen der Instanz mit einem Klick auf: **create database**

## Aufgabe 3 - Zur Datenbank verbinden

### VS Code öffnen

Öffnen Sie ein neues Browserfenster oder einen neuen Browser-Tab.
Navigieren Sie dort zu der Ihnen zugewiesenen VisualStudio Code Instanz unter `https://code-XX.{{ domain }}` (ersetzen Sie `XX` durch Ihre Nummer).

Melden Sie sich dort mit dem Passwort, welches Sie erhalten haben, an.

### Verbinden

Verbinden Sie sich über den Schreibenden Endpunkt mit dem **Datenbank Plugin** *oder* über das **mysql CLI-Tool** auf den Datenbankserver.

??? help "Datenbank Plugin - Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Klicken Sie in der linken Leiste auf das **Datenbank-Symbol** um das Datenbank Plugin zu starten.
    Am linken Bildschirmrand sollte sich der angezeigte Kontext ändern und Ihnen **Connections** im oberen Bereich anzeigen.
    Klicken Sie nun auf **Add new connection** und verwenden sie den **MySQL** Treiber.

    ??? summary "Verbindungseinstellungen"
        - Connection name: Geben Sie einen für Sie sinnvollen Namen ein.
        - Connect using: **Server and Port**
        - Server Address: Kopieren Sie den Schreib-Endpunkt aus der AWS RDS Instanz. Dieser ist in der Form `mydb.123456789012.eu-central-1.rds.amazonaws.com` angegeben.
        - Database: Geben Sie **employees** ein.
        - Username: Benutzen Sie den Usernamen aus dem [vorherigen Schritt](#mysql-instanz-in-rds-erstellen).
        - Use Password: **Save Password**
        - Password: Geben Sie das Passwort aus dem [vorherigen Schritt](#mysql-instanz-in-rds-erstellen) ein.

    Bestätigen Sie ihre Eingaben mit einem Klick auf **save connection**.
    Anschließend wird die neu erstellte Datenbankverbindung an der linken Seite unter **Connections** angezeigt.
    Bei einem Klick auf die Verbindung wird anschließend eine Verbindung aufgebaut und Sie können dann Datenbankqueries erstellen.

??? help "mysql CLI-Tool - Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Starten Sie ein neues Terminal indem Sie auf den Menü-Knopf (im linken oberen Eck) anklicken und dort **Terminal** :material-arrow-right: **New Terminal** auswählen. Am unteren Bildschirmrand ist nun ein Terminal zugänglich. Passen Sie nun diesen Befehl mit Ihren Daten auf um sich auf die Datenbank zu verbinden.

    ```bash
    mysql -u **Ihr User** -p -h **Endpunkt URL**
    ```

    Ihr Passwort für den Zugang wird dann interaktiv in der Kommandozeile abgefragt. Wenn alles richtig eingetragen wurde sollten Sie nun eine Wilkommensnachricht bekommen und mit der Datenbank verbunden sein.

    Beispiel Wilkommensnachricht:
    ```
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 31
    Server version: 5.7.12 MySQL Community Server (GPL)

    Copyright (c) 2000, 2022, Oracle and/or its affiliates.

    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

    mysql>
    ```

    Verlassen Sie anschließend das mysql Tool mit dem Befehl: `exit`

### Beispieldaten anlegen

Benutzen Sie das Script `run_load_data.sh` im VS Code um die Datenbank mit den Beispieldaten zu befüllen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Öffnen Sie den "File-Explorer" im Linken Bereich (unter dem Hauptmenü-Knopf).
    1. Öffnen Sie die Datei `run_load_data.sh` im Ordner `scripts`.
    1. Bearbeiten Sie die **Variables**: `USER=**Ihr DB User**`, `PW=**Ihr DB Passwort**`, `URL=**Url zum DB Endpunkt**`.
    1. Starten Sie ein neues Terminal indem Sie auf den Menü-Knopf (im linken oberen Eck) anklicken und dort **Terminal** :material-arrow-right: **New Terminal** auswählen. Am unteren Bildschirmrand ist nun ein Terminal zugänglich.
    1. Navigieren Sie im Terminal zum `scripts` Pfad mit dem Befehl:
        ```
        cd ~/workspace/scripts
        ```
    1. Führen Sie das Script aus mit dem Befehl:
        ```bash
        ./run_load_data.sh
        ```