# Cheatsheet 3 - Branches

## Part 1

| Command                                         | Action                                                                 |
|-------------------------------------------------|------------------------------------------------------------------------|
| ```git switch <branch>```                     | Switch to a local branch or get a remote branch and switch to it       |
| ```git switch -c <branch>```                  | Create new branch and switch to it                                     |
| ```git branch```                                | List available branches                                                |
| ```git branch -a```                             | List all available branches (also from remote)                         |
| ```git branch <branch>```                       | Create a new branch                                                    |
| ```git branch -d <branch>```                    | Delete the branch `<branch>`                                           |
| ```git fetch [<remote>] [<branch>]```           | Fetch changes from the remote                                          |
| ```git push [<remote>] [<branch>]```            | Push local changes to the remote server                                |
| ```git push --set-upstream <remote> <branch>``` | Push local branch to the remote server                                 |
| ```git merge <branch>```                        | Merge `<branch>` into the active branch                                |
| ```git merge --no--ff <branch>```               | Merge without fast-forward                                             |

## Part 2

| Command                           | Action                                               |
|-----------------------------------|------------------------------------------------------|
| ```git rebase <branch>```         | Move current branch onto `<branch>`                  |
| ```git pull --rebase <branch>```  | Get changes from server and rebase onto `<branch>`   |
| ```git stash```                   | Temporary put aside current uncommitted changes      |
| ```git stash pop```               | Get and apply last entry from stash                  |
