# Gitlab - Issues and Merge Requests

!!! important "Goal"
    - During this project you will learn how to collaborate with your colleagues in Gitlab.
    - These techniques are tranferable to other git services like GitHub or BitBucket.
    - The goal is to create an issue for a bug and to resolve this bug with the help of a merge request.
    
## Task 1

Look for the project `api-users` in Gitlab. This project
contains an API server for user management. But there is no documentation
at all. Create a new issue for adding the documentation. It is possible to
use Markdown in the description field. Start with the following example
and try some Markdown features. With a click on `Preview` you can have a look
on how it would be displayed later.

The following things should be documented:

- What the goal of this API is.
- How someone can use the API.
- How to setup and run the server.

In such an issue it is a good practice that a team of people will discuss on 
how to resolve it. We will now create a merge request and implement a solution there.

## Task 2

Create a merge request with a click on `Create merge request`. The new merge request
is now listed in the previously created issue and should be opened automatically.

Now we need to fill the merge request with relevant changes. For the
merge request Gitlab will automatically create a branch.

- Switch back to Visual Studio Code and clone the repository.
- Check out the branch from the merge request.
- Create and add a file `README.md` with a short descriptive text.
  Here it is also possible to use Markdown.
- Commit and push your changes to the server.

It is now possible to see your changes in the merge request in Gitlab.
Also your commit has started a pipeline run. This pipeline will check with
tests and linters if the code you committed is okay. In our case this is not
important since we did not change any source code. Otherwise for such code changes
the pipeline would give you a feedback for possible problems with your code. When you
are happy with the changes you made, you can click on on the three dots `...` and then 
click `mark as ready` in the right top corner to tell others that from your side 
this merge request is now ready to be reviewed. Sometimes only the button `mark as ready` is shown here.

- Click on the three dots `...` and then on `mark as ready`.

After that we need to apply those changes to `main`. Normally this would
be reviewed and approved by one of your colleagues.

- Click on `Merge when pipeline succeeds` to successfully close the merge
  request after the pipeline is done or click on `Close merge request` if the pipeline
  already ran successfully.

When the pipeline is done with success, the merge request will be closed and
this will also close the linked issue.

- Try to lookup the issue. It will now be shown in the 'closed' tab.

This is the whole workflow of an issue from start to the end.
